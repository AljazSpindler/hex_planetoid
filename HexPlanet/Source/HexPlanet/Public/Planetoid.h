// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "HexPlanetEnums.h"
#include "HexPlanetStructs.h"
#include "PlanetoidAbilityComponent.h"
#include "Gameplay/PlanetTriangle.h"
#include "Planetoid.generated.h"

UCLASS()
class HEXPLANET_API APlanetoid : public AActor
{
	GENERATED_BODY()

public:	

	APlanetoid();

	UPROPERTY(VisibleAnywhere, Category = "ProceduralPlanet")
	USceneComponent* PlanetRootComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UPlanetoidAbilityComponent* AbilityComponent;

	UPROPERTY()
	TArray<APlanetTriangle*> PlanetMeshes;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProceduralPlanet")
	UMaterialInterface* MeshMaterial;

	UPROPERTY(EditAnywhere, Category = "ProceduralPlanet")
	EPlanetoidGenerator GeneratorType = EPlanetoidGenerator::TRIANGLECLASS1;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "ProceduralPlanet")
	int32 Iterations = 0;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "ProceduralPlanet")
	float ScaleFactor = 100.0f;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "ProceduralPlanet")
	TArray<FVector> Vertices;

	UPROPERTY()
	TArray<int32> Triangles;

	UPROPERTY()
	TArray<FVector> Normals;

	UPROPERTY()
	TArray<FVector2D> UVS;

	UPROPERTY()
	TArray<FLinearColor> VertexColors;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "ProceduralPlanet")
	TArray<FHexTile> HexTiles;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "ProceduralPlanet")
	TArray<FTriFace> FaceDual;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "ProceduralPlanet")
	bool DebugNormals = false;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "ProceduralPlanet")
	bool DebugEdges = false;

	/* -------------------- */

	virtual void PostLoad() override;
	virtual void PostActorCreated() override;
#if WITH_EDITOR
	virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;
#endif // WITH_EDITOR

	void ClearProcMeshData();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:

	// Class 1 Triangle Geodesic Polyhedron Generator

	void GenerateMeshClass1();

	void SubdivideClass1(int32 Iter);

	int32 GetMidPointIndexClass1(TMap<int32, int32>& SubCache, int32 PointA, int32 PointB);

	void DetermineNeighbours();

	// Class 2 Triangle Geodesic Polyhedron Generator

	void GenerateMeshClass2();

	void SubdivideClass2(int32 Iter);

	bool CreateTrisFromEdgeClass2(TArray<TPair<int32, int32>> &EdgeDone, TArray<FTriFace> &TriList, int32 Tri, int32 OtherTri, int32 EdgeA, int32 EdgeB);

	void FindNeighboursClass2();

	bool EdgeMatchClass2(int32 A, int32 B, int32 OtherA, int32 OtherB, int32 OtherC);

	// Class 2 Hex Geodesic Polyhedron Generator

	void GenerateHexMeshClass2();

	// Builds pents and hexes from triangles.
	void BuildTiles();

	// Helpers
	void InitIcosahedronMesh();

	FTriFace GenerateTriangleFace(int32 Index1, int32 Index2, int32 Index3, int32 UID);

	void GenerateMeshComponentFromTriangle(FVector Vertex1, FVector Vertex2, FVector Vertex3, int32 UID, FTriFace* Face);

	void GenerateMeshComponentFromHex(FHexTile Tile, int32 UID);
};
