// Copyright (c) 2018+ True Imagination

#pragma once

#include "CoreMinimal.h"
#include "HexPlanetStructs.h"

/**
 * 
 */
class HEXPLANET_API TriAStarGraph
{
public:
	// Pointers
	TMap<FTriFace, float>* Cost;
	TArray<FTriFace>* Grid;
	TSet<FTriFace>* Blocked;

	TriAStarGraph(TMap<FTriFace, float>* InCost, TArray<FTriFace>* InGrid, TSet<FTriFace>* InBlocked) : Cost(InCost), Grid(InGrid), Blocked(InBlocked) {};

	typedef int32 FNodeRef;
	int32 GetNeighbourCount(const FNodeRef NodeRef) const;
	bool IsValidRef(const FNodeRef NodeRef) const;
	FNodeRef GetNeighbour(const FNodeRef NodeRef, const int32 NeighbourIndex) const;
};
