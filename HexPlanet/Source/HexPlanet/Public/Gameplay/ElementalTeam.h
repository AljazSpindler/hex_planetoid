// Copyright (c) 2018+ True Imagination

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "UnrealNetwork.h"
#include "HexPlanetPlayerState.h"
#include "HexPlanetEnums.h"
#include "Materials/Material.h"
#include "ElementalTeam.generated.h"

UCLASS()
class HEXPLANET_API AElementalTeam : public AActor
{
	GENERATED_BODY()
public:	
	AElementalTeam();
	
	UPROPERTY(Replicated, VisibleAnywhere, BlueprintReadOnly, Category = "HexPlanet|Team")
	EElement ElementType;

	UPROPERTY(Replicated, VisibleAnywhere, BlueprintReadOnly, Category = "HexPlanet|Team")
	FColor ColorRepresentation;

	UPROPERTY(Replicated, VisibleAnywhere, BlueprintReadOnly, Category = "HexPlanet|Team")
	UMaterialInterface* TeamMaterialReference;

	UFUNCTION(BlueprintCallable)
	FORCEINLINE TArray<AHexPlanetPlayerState*> GetTeamPlayers() { return TeamPlayers; }

	UFUNCTION(server, reliable, WithValidation)
	void AddPlayerToTeam(AHexPlanetPlayerState* ChosenPlayer);

	UFUNCTION(server, reliable, WithValidation)
	void RemovePlayerFromTeam(AHexPlanetPlayerState* ChosenPlayer);
private:
	UPROPERTY(Replicated, VisibleAnywhere, Category = "HexPlanet|Team")
	TArray<AHexPlanetPlayerState*> TeamPlayers;
};
