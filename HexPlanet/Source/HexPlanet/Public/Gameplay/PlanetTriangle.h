// Copyright (c) 2018+ True Imagination

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Materials/Material.h"
#include "HexPlanetEnums.h"
#include "UnrealNetwork.h"
#include "HexPlanetPlayerState.h"
#include "RuntimeMeshComponent.h"
#include "PlanetTriangle.generated.h"

UCLASS()
class HEXPLANET_API APlanetTriangle : public AActor
{
	GENERATED_BODY()
	
public:	
	APlanetTriangle();

protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	URuntimeMeshComponent* MeshComponent = nullptr;

	UPROPERTY(Replicated, VisibleAnywhere, BlueprintReadOnly)
	EElement ElementOwner = EElement::NEUTRAL;

	UPROPERTY(Replicated, VisibleAnywhere, BlueprintReadOnly)
	int UniqueTag;
	
	UPROPERTY(Replicated, VisibleAnywhere, BlueprintReadOnly)
	AHexPlanetPlayerState* LastPlayerTag = nullptr;

	UPROPERTY(ReplicatedUsing = OnRep_NewMaterial, BlueprintReadWrite) 
	UMaterialInterface* ActiveMaterial;

	UFUNCTION() 
	void OnRep_NewMaterial();

	UFUNCTION(NetMulticast, Reliable, WithValidation)
	void MultiCast_SetNewMaterial(UMaterialInterface* NewMat);
	
	UFUNCTION()
	void SetNewElementOwer(EElement NewOwner);
private:
	UFUNCTION()
	void SetNewMaterial(UMaterialInterface* NewMat);
};
