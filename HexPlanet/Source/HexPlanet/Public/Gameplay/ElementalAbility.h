// Copyright (c) 2018+ True Imagination

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "HexPlanetEnums.h"
#include "UnrealNetwork.h"
#include "Engine/Texture2D.h"
#include "ElementalAbility.generated.h"

UCLASS()
class HEXPLANET_API AElementalAbility : public AActor
{
	GENERATED_BODY()
	
public:	
	AElementalAbility();

protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(Replicated,EditDefaultsOnly, BlueprintReadOnly, Category = "HexPlanet")
	EElement ElementType = EElement::MAX;
	
	UPROPERTY(Replicated, EditDefaultsOnly, BlueprintReadOnly, Category = "HexPlanet")
	EAbilityType AbilityType = EAbilityType::MAX;

	UPROPERTY(Replicated, EditDefaultsOnly, BlueprintReadOnly, Category = "HexPlanet|Primary")
	FText AbilityName;

	UPROPERTY(Replicated, EditDefaultsOnly, BlueprintReadOnly, Category = "HexPlanet|Primary")
	FText AbilityDescription;

	UPROPERTY(Replicated, EditDefaultsOnly, BlueprintReadOnly, Category = "HexPlanet|Primary")
	float Cooldown = 1.0f;

	UPROPERTY(Replicated, EditDefaultsOnly, BlueprintReadOnly, Category = "HexPlanet|Primary")
	UTexture2D* AbilityIcon;

	UPROPERTY(Replicated, EditDefaultsOnly, BlueprintReadOnly, Category = "HexPlanet|Primary")
	bool bIsSpreadable;

	UPROPERTY(Replicated, EditDefaultsOnly, BlueprintReadOnly, Category = "HexPlanet|Targetted")
	bool bIsAProjectile = false;

	UPROPERTY(Replicated, EditDefaultsOnly, BlueprintReadOnly, Category = "HexPlanet|Targetted")
	float AbilityRadius = 0.0f;

	UPROPERTY(Replicated, EditDefaultsOnly, BlueprintReadOnly, Category = "HexPlanet|Spreadable", meta = (EditCondition = "bIsSpreadable"))
	int SpreadWaves = 0;

	UPROPERTY(Replicated, EditDefaultsOnly, BlueprintReadOnly, Category = "HexPlanet|Spreadable", meta = (EditCondition = "bIsSpreadable"))
	int TimeBetweenWaves = 0;

	UPROPERTY(Replicated, EditDefaultsOnly, BlueprintReadOnly, Category = "HexPlanet|Spreadable", meta = (EditCondition = "bIsSpreadable"))
	int CellsToSpread = 0;
};
