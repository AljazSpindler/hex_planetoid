// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "HexPlanetEnums.generated.h"

UENUM(BlueprintType)
enum class EElement : uint8
{
	Fire 	UMETA(DisplayName = "Fire"),
	Earth 	UMETA(DisplayName = "Earth"),
	Wind	UMETA(DisplayName = "Air"),
	Water   UMETA(DisplayName = "Water"),
	NEUTRAL   UMETA(DisplayName = "Neutral"),
	MAX UMETA(Hidden)
};

UENUM(BlueprintType)
enum class ECustomGameType : uint8
{
	TURN_BASED UMETA(DisplayName = "Turn Based"),
	REALTIME UMETA(DisplayName = "Real Time"),
	CHARACTER UMETA(DisplayName = "Character Based - Realtime"),
	MAX UMETA(Hidden)
};

UENUM(BlueprintType)
enum class EAbilityType : uint8
{
	MINOR UMETA(DisplayName = "Minor"),
	MAJOR UMETA(DisplayName = "Major"),
	UTILITY UMETA(DisplayName = "Utility"),
	ULTIMATE UMETA(DisplayName = "Ultimate"),
	MAX UMETA(Hidden)
};

UENUM(BlueprintType)
enum class eUINotifyEvent : uint8
{
	TURN_DONE,
	MAX
};

UENUM(BlueprintType)
enum class EPlanetoidGenerator : uint8
{
	TRIANGLECLASS1 UMETA(DisplayName = "Polyhedron Class 1 - Triangles"),
	TRIANGLECLASS2 UMETA(DisplayName = "Polyhedron Class 2 - Triangles"),
	HEXCLASS2 UMETA(DisplayName = "Polyhedron Class 2 - Hexes and Pents")
	/*
	HEXCLASS1 UMETA(DisplayName = "Geodesic Polyhedron Class 1 - Hexes and Pents"),
	*/
};

UCLASS()
class HEXPLANET_API UHexPlanetEnums : public UObject
{
	GENERATED_BODY()
	//reserved for to string conversions of enums due to unlocalizable enums Enum->FText where LOC is looked up	
};
