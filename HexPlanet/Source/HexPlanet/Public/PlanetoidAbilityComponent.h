// Copyright (c) 2018+ True Imagination

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "HexPlanetStructs.h"
#include "Runtime/AIModule/Public/GraphAStar.h"
#include "PlanetoidAbilityComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class HEXPLANET_API UPlanetoidAbilityComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UPlanetoidAbilityComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	bool bAllowInexact;

	/**
	 * AStar extension to find a path between StartPoint and EndPoint. The path is weighted with the great circle distance.
	 * @param StartPoint - Index of the starting element/node in the Grid.
	 * @param EndPoint - Index of the ending element/node in the grid.
	 * @param Grid - Input array of elements/nodes on which to search the path.
	 * @param Cost - Internal and out parameter that holds the costs of paths between elements/nodes.
	 * @param Blocked - An array of elements/nodes that can not be traversed.
	 * @param bSuccess - An out indicating if the pathfinding succeeded. True - Success, False - no path found.
	 * @param Results - An out array of indexes of elements/nodes that give the cheapest path from StartPoint to EndPoint.
	*/
	void AStarPathfinding(int32 StartPoint, int32 EndPoint, TArray<FTriFace> Grid, TMap<FTriFace, float> Cost, TSet<FTriFace> Blocked, bool& bSuccess, TArray<int32>& Results);

	/**
	* Takes Radius number of rings -> neigbours over the edges of the triangle.
	* @param StartPoint - Index of the starting element/node in the Grid.
	* @param ExcludeStartingPoint - If true excludes the StartPoint index from the Results.
	* @param Grid - Input array of elements/nodes on which to search the path.
	* @param Cost - Internal and out parameter that holds the costs of paths between elements/nodes.
	* @param Blocked - An array of elements/nodes that can not be traversed.
	* @param bSuccess - An out indicating if the ring-finding succeeded. True - Success, False - no ring found.
	* @param Results - An out array of indexes of elements/nodes that give ring around StartPoint.
	*/
	void EdgeRing(int32 StartPoint, bool ExcludeStartingPoint, int32 Radius, TArray<FTriFace> Grid, TSet<FTriFace> Blocked, bool& bSuccess, TArray<int32>& Results);

	/**
	* Takes Radius number of rings -> neigbours over the points of the triangle.
	* @param StartPoint - Index of the starting element/node in the Grid.
	* @param ExcludeStartingPoint - If true excludes the StartPoint index from the Results.
	* @param Grid - Input array of elements/nodes on which to search the path.
	* @param Cost - Internal and out parameter that holds the costs of paths between elements/nodes.
	* @param Blocked - An array of elements/nodes that can not be selected.
	* @param bSuccess - An out indicating if the ring-finding succeeded. True - Success, False - no ring found.
	* @param Results - An out array of indexes of elements/nodes that give the ring around StartPoint.
	*/
	void PointRing(int32 StartPoint, bool ExcludeStartingPoint, int32 Radius, TArray<FTriFace> Grid, TSet<FTriFace> Blocked, bool& bSuccess, TArray<int32>& Results);

	/**
	* Takes the opposite point on the planet piercing it through to the other side.
	* @param StartPoint - Index of the starting element/node in the Grid.
	* @param Grid - Input array of elements/nodes on which to search the path.
	* @param Blocked - An array of elements/nodes that can not be selected.
	* @param bSuccess - An out indicating if the pierce succeeded. True - Success, False - no pierce found.
	* @param Result - An out index of the element/node that gives the pierce planet index.
	*/
	void PiercePlanet(int32 StartPoint, TArray<FTriFace> Grid, TSet<FTriFace> Blocked, bool& bSuccess, int32& Result);
	
};