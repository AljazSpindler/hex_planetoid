// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "HexPlanetEnums.h"
#include "Runtime/AIModule/Public/GraphAStar.h"
#include "HexPlanetStructs.generated.h"

class AElementalTeam;
class AHexPlanetPlayerState;

USTRUCT(BlueprintType)
struct FTriFace
{
	GENERATED_BODY()

	FTriFace();

	FTriFace(int32 A, int32 B, int32 C);

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "HexPlanet|Face")
	int32 UID;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "HexPlanet|Face")
	int32 PointA;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "HexPlanet|Face")
	int32 PointB;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "HexPlanet|Face")
	int32 PointC;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "HexPlanet|Face")
	FVector Centroid;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "HexPlanet|Face")
	FVector Normal;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "HexPlanet|Face")
	FRotator Rotation;

	TArray<int32> EdgeNeighbours;
	TArray<int32> VertNeighbours;

	int32 ABNeighbour;
	int32 BCNeighbour;
	int32 CANeighbour;

	int32 NewVert;
	float Angle;

	FVector GetCentroidClass1(TArray<FVector> VertexArray);
	FVector GetCenterClass2(TArray<struct FHexTile> HexTiles);
	FVector GetNormalClass1(TArray<FVector> VertexArray);
	FRotator GetRotationClass1(USceneComponent* Root);

	bool IsEdgeNeighbourOf(FTriFace& OtherFace);
	bool IsVertNeighbourOf(FTriFace& OtherFace);

	bool operator==(const FTriFace& Other) const { return UID == Other.UID; }
	bool operator!=(const FTriFace& Other) const { return UID != Other.UID; }

};

FORCEINLINE uint32 GetTypeHash(const FTriFace& b)
{
	return FCrc::MemCrc32(&b, sizeof(FTriFace));
}

USTRUCT(BlueprintType)
struct FHexTile
{
	GENERATED_BODY()

	FHexTile();

	FHexTile(FVector Pos);

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "HexPlanet|Tile")
	int32 UID;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "HexPlanet|Tile")
	FVector Position;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "HexPlanet|Tile")
	FVector Normal;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "HexPlanet|Tile")
	EElement Element;

	TArray<int32> HexTris;

	TArray<int32> HexVertices;

	int32 CenterVertexIndex = -1;

	bool operator==(const FHexTile& Other) const { return UID == Other.UID; }
	bool operator!=(const FHexTile& Other) const { return UID != Other.UID; }

	bool IsTileNeighbourOf(FHexTile& OtherTile);
	bool IsContainingTile(FHexTile& OtherTile);
};

USTRUCT(BlueprintType)
struct FHexVert
{
	GENERATED_BODY();

	FHexVert();

	FHexVert(int32 Ind, FVector Pos);

	UPROPERTY()
	int32 Index;

	UPROPERTY()
	FVector Position;

	TArray<int32> TileNeighbours;
};

USTRUCT(BlueprintType)
struct FTurnBased_GameStatus
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "HexPlanet|Gameplay")
	AElementalTeam* ActiveTeamTurn = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "HexPlanet|Gameplay")
	AHexPlanetPlayerState* ActivePlayer = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "HexPlanet|Gameplay")
	int32 TurnNumber = 1;
};

USTRUCT(BlueprintType)
struct FPlayerScore
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "HexPlanet|Gameplay")
	FString PlayerName;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "HexPlanet|Gameplay")
	int32 Score;
};

USTRUCT(BlueprintType)
struct FTeamScore
{
	GENERATED_BODY()
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "HexPlanet|Gameplay")
	EElement TeamElement;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "HexPlanet|Gameplay")
	int32 TotalScore;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "HexPlanet|Gameplay")
	TArray<FPlayerScore> PlayerScores;

	FTeamScore()
	{
		TeamElement = EElement::MAX;
	}
};

USTRUCT(BlueprintType)
struct FAbilityCooldown
{
	GENERATED_BODY()
	UPROPERTY()
	EAbilityType AbilityType;

	UPROPERTY()
	float CooldownTimeRemaining = 0.0f;
};

UCLASS()
class HEXPLANET_API UHexPlanetStructs : public UObject
{
	GENERATED_BODY()	
};
