// Copyright (c) 2018+ True Imagination

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "HexPlanetEnums.h"
#include "HexPlanetPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class HEXPLANET_API AHexPlanetPlayerController : public APlayerController
{
	GENERATED_BODY()
public:	
	UFUNCTION(BlueprintImplementableEvent) void NotifyUI(eUINotifyEvent curEvent);
};
