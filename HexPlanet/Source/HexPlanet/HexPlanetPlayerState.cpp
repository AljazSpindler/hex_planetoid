// Copyright (c) 2018+ True Imagination

#include "HexPlanetPlayerState.h"
#include "HexPlanetGameState.h"
#include "Engine/World.h"

AHexPlanetPlayerState::AHexPlanetPlayerState()
{
	bAlwaysRelevant = true;
	bReplicates = true;
}

void AHexPlanetPlayerState::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(AHexPlanetPlayerState, PawnRef);
}


void AHexPlanetPlayerState::BeginPlay()
{
	Super::BeginPlay();
	
	//for now just add it to team FIRE
	AHexPlanetGameState* GS = Cast<AHexPlanetGameState>(GetWorld()->GetGameState());

	if (GS)
	{
		int RandInt = FMath::RandRange(0, GS->GetTeams().Num()-1);
		EElement curElement = EElement(RandInt);
		GS->AddPlayerToTeam(curElement, this);
	}
}

bool AHexPlanetPlayerState::Server_SetPawnReference_Validate(AHexPlanetCharacter* curChar) { return true; }
void AHexPlanetPlayerState::Server_SetPawnReference_Implementation(AHexPlanetCharacter* curChar)
{
	PawnRef = curChar;
}