// Copyright (c) 2018+ True Imagination

#include "HexPlanetCharacter.h"
#include "Engine/World.h"
#include "HexPlanetGameState.h"
#include "Gameplay/ElementalAbility.h"
#include "Components/CapsuleComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "Components/SphereComponent.h"
#include "Gameplay/PlanetTriangle.h"
#include "Planetoid.h"
#include "EngineUtils.h"
#include "DrawDebugHelpers.h"
#include "Math/UnrealMathUtility.h"

AHexPlanetCharacter::AHexPlanetCharacter()
{
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;
	bReplicateMovement = true;

	RootScene = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = RootScene;
	
	SpringArmX = CreateDefaultSubobject<USpringArmComponent>(TEXT("Spring Arm X"));
	SpringArmX->SetupAttachment(RootComponent);
	SpringArmX->TargetArmLength = 0.0f;
	SpringArmX->bDoCollisionTest = false;

	SpringArmY = CreateDefaultSubobject<USpringArmComponent>(TEXT("Spring Arm Y"));
	SpringArmY->SetupAttachment(SpringArmX);
	SpringArmY->TargetArmLength = 500.0f;
	SpringArmY->bDoCollisionTest = false;

	CameraComp = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	CameraComp->SetupAttachment(SpringArmY);
}

void AHexPlanetCharacter::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(AHexPlanetCharacter, SpringArmX);
	DOREPLIFETIME(AHexPlanetCharacter, SpringArmY);
	DOREPLIFETIME(AHexPlanetCharacter, CameraComp);
	DOREPLIFETIME(AHexPlanetCharacter, GS);
	DOREPLIFETIME(AHexPlanetCharacter, Cooldowns);
	DOREPLIFETIME(AHexPlanetCharacter, bRightMouseDown);	
}

void AHexPlanetCharacter::BeginPlay()
{
	Super::BeginPlay();
	GS = Cast<AHexPlanetGameState>(GetWorld()->GetGameState());
	SafeBeginPlay();

	if (GS)
	{
		if (GS->ActiveGameType == ECustomGameType::REALTIME)
		{
			for (int i = 0; i < (uint8)EAbilityType::MAX; i++)
			{
				FAbilityCooldown curCD;
				curCD.AbilityType = EAbilityType(i);
				curCD.CooldownTimeRemaining = 0.0f;
				Cooldowns.Add(curCD);
			}
		}
	}
}

float AHexPlanetCharacter::GetCooldown(EAbilityType AbilityType)
{
	for (int i = 0; i < Cooldowns.Num(); i++)
	{
		if (Cooldowns[i].AbilityType == AbilityType)
		{
			return Cooldowns[i].CooldownTimeRemaining;
		}
	}

	return 999.9f;
}

void AHexPlanetCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (PlayerState != nullptr)
	{
		AHexPlanetPlayerState* curPS = Cast<AHexPlanetPlayerState>(PlayerState);
		if (curPS && curPS->PawnRef == nullptr)
		{
			curPS->Server_SetPawnReference(this);
		}
	}

	if (GS)
	{
		if (GS->ActiveGameType == ECustomGameType::REALTIME)
		{
			ReduceCooldownTimes(DeltaTime);
		}
	}
}

void AHexPlanetCharacter::ReduceCooldownTimes(float DeltaTime)
{
	for (int i = 0; i < Cooldowns.Num(); i++)
	{
		Cooldowns[i].CooldownTimeRemaining -= DeltaTime;
		Cooldowns[i].CooldownTimeRemaining = FMath::Clamp(Cooldowns[i].CooldownTimeRemaining, 0.0f, 999.9f);
	}
}

void AHexPlanetCharacter::ApplyCooldownTime(AElementalAbility* curAbility)
{
	for (int i = 0; i < Cooldowns.Num(); i++)
	{
		if(Cooldowns[i].AbilityType == curAbility->AbilityType)
		{
			Cooldowns[i].CooldownTimeRemaining = curAbility->Cooldown;
		}
	}
}

void AHexPlanetCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("Turn", this, &AHexPlanetCharacter::MouseX_Movement);
	PlayerInputComponent->BindAxis("LookUp", this, &AHexPlanetCharacter::MouseY_Movement);
	PlayerInputComponent->BindAction("MinorAbility", IE_Pressed, this, &AHexPlanetCharacter::FirePrimaryAbility);
	PlayerInputComponent->BindAction("MajorAbility", IE_Pressed, this, &AHexPlanetCharacter::FireMajorAbility);
	PlayerInputComponent->BindAction("UtilityAbility", IE_Pressed, this, &AHexPlanetCharacter::FireUtilityAbility);
	PlayerInputComponent->BindAction("UltimateAbility", IE_Pressed, this, &AHexPlanetCharacter::FireUltimateAbility);
	PlayerInputComponent->BindAction("MouseScrollUp", IE_Pressed, this, &AHexPlanetCharacter::MouseScrollUp);
	PlayerInputComponent->BindAction("MouseScrollDown", IE_Pressed, this, &AHexPlanetCharacter::MouseScrollDown);
	PlayerInputComponent->BindAction("MouseRightClick", IE_Pressed, this, &AHexPlanetCharacter::RightMouseClickPressed);
	PlayerInputComponent->BindAction("MouseRightClick", IE_Released, this, &AHexPlanetCharacter::RightMouseClickReleased);
}

void AHexPlanetCharacter::MouseScrollUp()
{
	float ScrollStrength = 1000.0f;
	float ScrollAmount = ScrollStrength * GetWorld()->GetDeltaSeconds();
	float NewAmount = SpringArmY->TargetArmLength - ScrollAmount;

	if (NewAmount > 0)
	{
		SpringArmY->TargetArmLength -= ScrollAmount;
	}
}

void AHexPlanetCharacter::MouseScrollDown()
{
	float ScrollStrength = 1000.0f;
	float ScrollAmount = ScrollStrength * GetWorld()->GetDeltaSeconds();
	float NewAmount = SpringArmY->TargetArmLength - ScrollAmount;

	if (NewAmount < 500)
	{
		SpringArmY->TargetArmLength += ScrollAmount;
	}
	
}

void AHexPlanetCharacter::RightMouseClickPressed()
{
	Server_RightMouseClick(true);
}

void AHexPlanetCharacter::RightMouseClickReleased()
{
	Server_RightMouseClick(false);
}

bool AHexPlanetCharacter::Server_RightMouseClick_Validate(bool bPressedDown) { return true; }
void AHexPlanetCharacter::Server_RightMouseClick_Implementation(bool bPressedDown)
{
	bRightMouseDown = bPressedDown;
}

void AHexPlanetCharacter::MouseX_Movement(float MoveAmount)
{
	if (GS)
	{
		if (GS->ActiveGameType != ECustomGameType::CHARACTER && bRightMouseDown)
		{
			if (MoveAmount > 0 || MoveAmount < 0)
			{
				float RotationSpeed = 150.0f ;
				FVector WorldLocation = this->GetActorLocation();
				FVector OrbitingAround = this->CameraTargetLocation;

				float CameraMoveAmount = MoveAmount * RotationSpeed * GetWorld()->GetDeltaSeconds();
				FRotator NewRotation = FRotator(0.0f, CameraMoveAmount, 0.0f);
				SpringArmX->AddWorldRotation(NewRotation, true);
			}
		}
	}
}

void AHexPlanetCharacter::MouseY_Movement(float MoveAmount)
{
	if (GS)
	{
		if (GS->ActiveGameType != ECustomGameType::CHARACTER && bRightMouseDown)
		{
			if (MoveAmount > 0 || MoveAmount < 0)
			{
				float RotationSpeed = 150.0f;
				FVector WorldLocation = this->GetActorLocation();
				FVector OrbitingAround = this->CameraTargetLocation;

				float CameraMoveAmount = MoveAmount * RotationSpeed * GetWorld()->GetDeltaSeconds();
				FRotator NewRotation = FRotator(CameraMoveAmount, 0.0f, 0.0f);

				float FinalRotation = SpringArmY->GetComponentRotation().Pitch + CameraMoveAmount;

				SpringArmY->AddLocalRotation(NewRotation, true);			
			}
		}
	}
}

void AHexPlanetCharacter::CallNextTurn()
{
	Server_CallNextTurn();
}

bool AHexPlanetCharacter::Server_CallNextTurn_Validate()
{
	return true;
}

void AHexPlanetCharacter::Server_CallNextTurn_Implementation()
{
	if (GS)
	{
		GS->Server_NextPlayerTurn();
	}
}

void AHexPlanetCharacter::FirePrimaryAbility()
{
	AElementalAbility* curAbility = nullptr;
	AElementalTeam* curTeam = GetTeam();
	
	curAbility = GS->FindAbility(EAbilityType::MINOR, curTeam->ElementType);
	
	if (curAbility)
	{
		StartAbility(curAbility);
	}
}

void AHexPlanetCharacter::FireMajorAbility()
{
	AElementalAbility* curAbility = nullptr;
	AElementalTeam* curTeam = GetTeam();

	curAbility = GS->FindAbility(EAbilityType::MAJOR, curTeam->ElementType);

	if (curAbility)
	{
		StartAbility(curAbility);
	}
}

void AHexPlanetCharacter::FireUtilityAbility()
{
	AElementalAbility* curAbility = nullptr;
	AElementalTeam* curTeam = GetTeam();

	curAbility = GS->FindAbility(EAbilityType::UTILITY, curTeam->ElementType);

	if (curAbility)
	{
		StartAbility(curAbility);
	}
}

void AHexPlanetCharacter::FireUltimateAbility()
{
	AElementalAbility* curAbility = nullptr;
	AElementalTeam* curTeam = GetTeam();

	curAbility = GS->FindAbility(EAbilityType::ULTIMATE, curTeam->ElementType);

	if (curAbility)
	{
		StartAbility(curAbility);
	}
}

void AHexPlanetCharacter::StartAbility(AElementalAbility* curAbility)
{
	if (!GS)
	{
		return;
	}

	if (GS->ActiveGameType == ECustomGameType::TURN_BASED)
	{
		if (GS->TurnBased_GameStatus.ActivePlayer != Cast<AHexPlanetPlayerState>(this->GetController()->PlayerState))
		{
			return;
		}
	}
	else if (GS->ActiveGameType == ECustomGameType::REALTIME)
	{
		//No special rules
	}

	FVector StartLocation = this->CameraComp->GetComponentLocation();
	FVector EndLocation = this->GetActorLocation();

	Server_FireAbility(curAbility->ElementType, curAbility->AbilityType, StartLocation, EndLocation);
}

void AHexPlanetCharacter::CancelAbility()
{
	//NYI
}

AElementalTeam* AHexPlanetCharacter::GetTeam()
{
	if (GS)
	{
		return GS->FindPlayerTeam(Cast<AHexPlanetPlayerState>(PlayerState));
	}

	return nullptr;
}

bool AHexPlanetCharacter::Server_FireAbility_Validate(EElement ElementType, EAbilityType AbilityType, FVector StartLocation, FVector EndLocation)
{
	return true;
}
void AHexPlanetCharacter::Server_FireAbility_Implementation(EElement ElementType, EAbilityType AbilityType, FVector StartLocation, FVector EndLocation)
{
	if (GS)
	{
			AElementalAbility* curAbility = GS->FindAbility(AbilityType, ElementType);

			float curCD = GetCooldown(curAbility->AbilityType);

			if (curAbility && curCD <= 0.0f)
			{		
				if (curAbility->bIsAProjectile == false)
				{
					NonProjectileAbilityHandling(curAbility, StartLocation, EndLocation);
				}
				else
				{
					ProjectileAbilityHandling(curAbility);
				}
			}
					
	}
}

void AHexPlanetCharacter::ProjectileAbilityHandling(AElementalAbility* curAbility)
{
	//NYI
}

void AHexPlanetCharacter::NonProjectileAbilityHandling(AElementalAbility* curAbility, FVector StartLocation, FVector EndLocation)
{
	//should always be called from authority
	if (HasAuthority())
	{
		AElementalTeam* curTeam = GetTeam();

		if (curAbility->AbilityRadius > 0.0f)
		{
			FCollisionQueryParams TraceParams(FName(TEXT("Primary Ability Trace")), true, nullptr);
			TraceParams.bReturnPhysicalMaterial = false;
			TraceParams.AddIgnoredActor(this);
			FHitResult HitOut = FHitResult(ForceInit);

			GetWorld()->SweepSingleByChannel(
				HitOut,
				StartLocation,
				EndLocation,
				FQuat(),
				ECollisionChannel::ECC_Visibility,
				FCollisionShape::MakeSphere(curAbility->AbilityRadius),
				TraceParams
			);

			if (HitOut.bBlockingHit)
			{
				DrawDebugLine(GetWorld(), StartLocation, HitOut.ImpactPoint, GetTeam()->ColorRepresentation, false, 3.0f, 0, 2.0f);
				DrawDebugSphere(GetWorld(), HitOut.ImpactPoint, curAbility->AbilityRadius, 16, GetTeam()->ColorRepresentation, false, 3.0f, 0, 2.0f);
				TArray<FOverlapResult> Overlaps;
				GetWorld()->OverlapMultiByChannel(Overlaps, HitOut.ImpactPoint, FQuat(), ECollisionChannel::ECC_Visibility, FCollisionShape::MakeSphere(curAbility->AbilityRadius), TraceParams);

				if (Overlaps.Num() > 0)
				{
					for (int i = 0; i < Overlaps.Num(); i++)
					{
						FOverlapResult curResult = Overlaps[i];

						APlanetoid* curPlanetoid = Cast<APlanetoid>(curResult.Actor);
						URuntimeMeshComponent* curTriangleComp = Cast<URuntimeMeshComponent>(curResult.Component);

						if (curTriangleComp)
						{
							if (curTeam)
							{
								UE_LOG(LogTemp, Warning, TEXT("The Planet component %s was shit! "), *curTriangleComp->GetName());
								UMaterialInstanceDynamic* CompMat = UMaterialInstanceDynamic::Create(curTeam->TeamMaterialReference, this);
								APlanetTriangle* curTriangle = Cast<APlanetTriangle>(curTriangleComp->GetOwner());

								if (curTriangle)
								{
									EElement curElementOwner = curTriangle->ElementOwner;
									curTriangle->SetNewElementOwer(curTeam->ElementType);
								}
							}
						}
					}
				}

				if (GS->ActiveGameType == ECustomGameType::TURN_BASED)
				{
					if (GS->TurnBased_GameStatus.ActiveTeamTurn == nullptr)
					{
						return;
					}
					GS->Server_NextPlayerTurn();
				}
			}
		}

		//Handle spreading
		if(curAbility->bIsSpreadable)
		{
			TArray<APlanetTriangle*> MeshList = GS->GetAllTeamOwnedMeshes(curTeam->ElementType);
		}

		ApplyCooldownTime(curAbility);
		GS->CalculateScores();
	}
}
