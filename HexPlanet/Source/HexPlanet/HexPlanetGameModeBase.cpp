// Fill out your copyright notice in the Description page of Project Settings.

#include "HexPlanetGameModeBase.h"
#include "HexPlanetGameState.h"
#include "HexPlanetPlayerState.h"
#include "HexPlanetCharacter.h"
#include "HexPlanetPlayerController.h"

AHexPlanetGameModeBase::AHexPlanetGameModeBase()
{
	GameStateClass = AHexPlanetGameState::StaticClass();
	PlayerStateClass = AHexPlanetPlayerState::StaticClass();
	DefaultPawnClass = AHexPlanetCharacter::StaticClass();
	PlayerControllerClass = AHexPlanetPlayerController::StaticClass();
}


