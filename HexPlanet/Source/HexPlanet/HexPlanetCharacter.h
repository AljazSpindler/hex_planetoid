// Copyright (c) 2018+ True Imagination

#pragma once

#include "CoreMinimal.h"
#include "UnrealNetwork.h"
#include "GameFramework/Character.h"
#include "Camera/CameraComponent.h"
#include "GameFrameWork/SpringArmComponent.h"
#include "HexPlanetEnums.h"
#include "HexPlanetStructs.h"
#include "HexPlanetCharacter.generated.h"

class AHexPlanetGameState;

UCLASS()
class HEXPLANET_API AHexPlanetCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	AHexPlanetCharacter();
	USceneComponent* RootScene;

	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite)
	USpringArmComponent* SpringArmX;

	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite)
	USpringArmComponent* SpringArmY;

	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite)
	UCameraComponent* CameraComp;

	UPROPERTY(Replicated, VisibleAnywhere, BlueprintReadOnly)
	AHexPlanetGameState* GS;

	UPROPERTY(Replicated, VisibleAnywhere, BlueprintReadOnly)
	bool bRightMouseDown = false;
protected:
	virtual void BeginPlay() override;
private:
	void RightMouseClickPressed();
	void RightMouseClickReleased();

	UFUNCTION(server, reliable, WithValidation)
	void Server_RightMouseClick(bool bPressedDown);

public:	
	virtual void Tick(float DeltaTime) override;
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	//Basic
	UFUNCTION(BlueprintImplementableEvent) 
	void SafeBeginPlay();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly) 
	FVector CameraTargetLocation = FVector(0.0f, 200.0f, 150.0f);


	//Camera
	UFUNCTION() 
	void MouseX_Movement(float MoveAmount);

	UFUNCTION() 
	void MouseY_Movement(float MoveAmount);

	UFUNCTION() 
	void MouseScrollUp();

	UFUNCTION() 
	void MouseScrollDown();

	//Gameplay
	///Turn Based
	UFUNCTION(BlueprintCallable)
	void CallNextTurn(); ///TEMPORARY

	UFUNCTION(server, reliable, WithValidation) 
	void Server_CallNextTurn();

	///Realmtime
	UPROPERTY(Replicated)
	TArray<FAbilityCooldown> Cooldowns;

	UFUNCTION(BlueprintCallable)
	float GetCooldown(EAbilityType AbilityType);

	UFUNCTION()
	void ReduceCooldownTimes(float DeltaTime);

	UFUNCTION()
	void ApplyCooldownTime(AElementalAbility* curAbility);

	///Generic
	UFUNCTION(BlueprintCallable) 
	void FirePrimaryAbility();

	UFUNCTION(BlueprintCallable)
	void FireMajorAbility();

	UFUNCTION(BlueprintCallable)
	void FireUtilityAbility();

	UFUNCTION(BlueprintCallable)
	void FireUltimateAbility();

	UFUNCTION(server, reliable, WithValidation)
	void Server_FireAbility(EElement ElementType, EAbilityType AbilityType, FVector StartLocation, FVector EndLocation);

	UFUNCTION()
	void NonProjectileAbilityHandling(AElementalAbility* curAbility, FVector StartLocation, FVector EndLocation);

	UFUNCTION()
	void ProjectileAbilityHandling(AElementalAbility* curAbility);

	UFUNCTION(BlueprintCallable)
	AElementalTeam* GetTeam();

	UFUNCTION()
	void StartAbility(AElementalAbility* curAbility);

	UFUNCTION()
	void CancelAbility();
};
