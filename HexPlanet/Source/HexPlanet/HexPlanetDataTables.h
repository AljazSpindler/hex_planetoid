// Copyright (c) 2018+ True Imagination

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "HexPlanetEnums.h"
#include "Materials/Material.h"
#include "Engine/DataTable.h"
#include "Gameplay/ElementalAbility.h"
#include "HexPlanetDataTables.generated.h"

USTRUCT(BlueprintType)
struct FTeamMaterialsRow : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Team Materials")
	EElement ElementType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Team Materials")
	UMaterialInterface* MaterialReferenece;
};

USTRUCT(BlueprintType)
struct FAbilityRow : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ability")
	EElement ElementType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ability")
	EAbilityType AbilityType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ability")
	TSubclassOf<AElementalAbility> BPRef;
};

UCLASS()
class HEXPLANET_API UHexPlanetDataTables : public UObject
{
	GENERATED_BODY()
};
