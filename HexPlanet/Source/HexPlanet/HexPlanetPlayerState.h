// Copyright (c) 2018+ True Imagination

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "HexPlanetPlayerState.generated.h"

class AHexPlanetCharacter;

UCLASS()
class HEXPLANET_API AHexPlanetPlayerState : public APlayerState
{
	GENERATED_BODY()
public:
	AHexPlanetPlayerState();
protected:
	virtual void BeginPlay() override;
public:	
	UPROPERTY(Replicated, BlueprintReadOnly, VisibleAnywhere)
	AHexPlanetCharacter* PawnRef = nullptr;	

	UFUNCTION(Server, reliable, WithValidation)
	void Server_SetPawnReference(AHexPlanetCharacter* curChar);
};
