// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "HexPlanetGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class HEXPLANET_API AHexPlanetGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	AHexPlanetGameModeBase();
};
