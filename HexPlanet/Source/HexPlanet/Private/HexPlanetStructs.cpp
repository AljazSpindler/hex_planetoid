// Fill out your copyright notice in the Description page of Project Settings.

#include "HexPlanetStructs.h"
#include "Components/SceneComponent.h"

FHexTile::FHexTile()
{
	Position = FVector(0.0f);
	Normal = Position.GetSafeNormal();
	Element = EElement::NEUTRAL;
}

FHexTile::FHexTile(FVector Pos)
{
	Position = Pos;
	Normal = Pos.GetSafeNormal();
	Element = EElement::NEUTRAL;
}

FTriFace::FTriFace()
{
	PointA = 0;
	PointB = 0;
	PointC = 0;

	ABNeighbour = -1;
	BCNeighbour = -1;
	CANeighbour = -1;

	Angle = 0.0f;
	NewVert = TNumericLimits<int32>::Max();
}

FTriFace::FTriFace(int32 A, int32 B, int32 C)
{
	PointA = A;
	PointB = B;
	PointC = C;

	ABNeighbour = -1;
	BCNeighbour = -1;
	CANeighbour = -1;

	Angle = 0.0f;
	NewVert = TNumericLimits<int32>::Max();
}

FVector FTriFace::GetCentroidClass1(TArray<FVector> VertexArray)
{
	FVector CenterPoint = VertexArray[PointA];
	CenterPoint += VertexArray[PointB];
	CenterPoint += VertexArray[PointC];
	CenterPoint /= 3.0f;

	return CenterPoint;
}

FVector FTriFace::GetCenterClass2(TArray<struct FHexTile> HexTiles)
{
	FVector CenterPoint = HexTiles[PointA].Position;
	CenterPoint += HexTiles[PointB].Position;
	CenterPoint += HexTiles[PointC].Position;
	CenterPoint /= 3.0f;

	return CenterPoint;
}

FVector FTriFace::GetNormalClass1(TArray<FVector> VertexArray)
{
	FVector U = VertexArray[PointB] - VertexArray[PointA];
	FVector V = VertexArray[PointC] - VertexArray[PointA];

	return FVector::CrossProduct(U.GetSafeNormal(), V.GetSafeNormal()).GetSafeNormal();
}

FRotator FTriFace::GetRotationClass1(USceneComponent* Root)
{
	FVector UpVector = Root->GetUpVector();
	FVector NormalVector = Normal;

	FVector RotationAxis = FVector::CrossProduct(UpVector, NormalVector);
	RotationAxis.Normalize();

	float DotProduct = FVector::DotProduct(UpVector, NormalVector);
	float RotationAngle = acosf(DotProduct);

	FQuat Quat = FQuat(RotationAxis, RotationAngle);
	FQuat RootQuat = Root->GetComponentQuat();

	FQuat NewQuat = Quat * RootQuat;

	return NewQuat.Rotator();
}

bool FTriFace::IsEdgeNeighbourOf(FTriFace& OtherFace)
{
	int32 SharedVertices = 0;
	TArray<int32> CurrentVertices;
	CurrentVertices.Add(PointA); CurrentVertices.Add(PointB); CurrentVertices.Add(PointC);

	TArray<int32> OtherVertices;
	OtherVertices.Add(OtherFace.PointA); OtherVertices.Add(OtherFace.PointB); OtherVertices.Add(OtherFace.PointC);

	TArray<int32> SharedVerticesIndex;

	for (int32 Vert : CurrentVertices)
	{
		if (OtherVertices.Contains(Vert))
		{
			++SharedVertices;
			SharedVerticesIndex.Add(Vert);
		}
	}

	if (SharedVertices == 2)
	{
		if ((SharedVerticesIndex[0] == PointA && SharedVerticesIndex[1] == PointB)
			|| (SharedVerticesIndex[1] == PointA && SharedVerticesIndex[0] == PointB)
		   )
		{
			ABNeighbour = OtherFace.UID;
		}
		else if ((SharedVerticesIndex[0] == PointB && SharedVerticesIndex[1] == PointC)
			     || (SharedVerticesIndex[1] == PointB && SharedVerticesIndex[0] == PointC)
			    )
		{
			BCNeighbour = OtherFace.UID;
		}
		else if ((SharedVerticesIndex[0] == PointC && SharedVerticesIndex[1] == PointA)
			     || (SharedVerticesIndex[1] == PointC && SharedVerticesIndex[0] == PointA)
			    )
		{
			CANeighbour = OtherFace.UID;
		}
		return true;
	}

	return false;
}

bool FTriFace::IsVertNeighbourOf(FTriFace& OtherFace)
{
	int32 SharedVertices = 0;
	TArray<int32> CurrentVertices;
	CurrentVertices.Add(PointA); CurrentVertices.Add(PointB); CurrentVertices.Add(PointC);

	TArray<int32> OtherVertices;
	OtherVertices.Add(OtherFace.PointA); OtherVertices.Add(OtherFace.PointB); OtherVertices.Add(OtherFace.PointC);

	for (int32 Vert : CurrentVertices)
	{
		if (OtherVertices.Contains(Vert))
		{
			++SharedVertices;
		}
	}

	if (SharedVertices > 0)
	{
		return true;
	}

	return false;
}

FHexVert::FHexVert()
{
	Index = -1;
}

FHexVert::FHexVert(int32 Ind, FVector Pos)
{
	Index = Ind;
	Position = Pos;
}

bool FHexTile::IsTileNeighbourOf(FHexTile& OtherTile)
{
	if (HexVertices.Contains(OtherTile.CenterVertexIndex))
	{
		return false;
	}
	else
	{
		int32 Cnt = 0;
		for (int32 OtherVert : OtherTile.HexVertices)
		{
			if (HexVertices.Contains(OtherVert))
			{
				Cnt++;
			}
			if (Cnt == 2) // We have a shared edge, so it must be a neighbour.
			{
				return true;
			}
		}
	}

	return false;
}

bool FHexTile::IsContainingTile(FHexTile& OtherTile)
{
	if (HexVertices.Contains(OtherTile.CenterVertexIndex))
	{
		return true;
	}
	return false;
}


