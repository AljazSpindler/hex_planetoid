// Copyright (c) 2018+ True Imagination

#include "ElementalAbility.h"

AElementalAbility::AElementalAbility()
{
	PrimaryActorTick.bCanEverTick = true;
	bAlwaysRelevant = true;
	bReplicates = true;
}

void AElementalAbility::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(AElementalAbility, ElementType);
	DOREPLIFETIME(AElementalAbility, AbilityType);
	DOREPLIFETIME(AElementalAbility, AbilityRadius);
	DOREPLIFETIME(AElementalAbility, AbilityDescription);
	DOREPLIFETIME(AElementalAbility, Cooldown);
	DOREPLIFETIME(AElementalAbility, bIsAProjectile);
	DOREPLIFETIME(AElementalAbility, AbilityIcon);
}


void AElementalAbility::BeginPlay()
{
	Super::BeginPlay();
}

void AElementalAbility::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

