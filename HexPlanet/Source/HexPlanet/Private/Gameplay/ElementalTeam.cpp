// Copyright (c) 2018+ True Imagination

#include "ElementalTeam.h"

AElementalTeam::AElementalTeam()
{
	PrimaryActorTick.bCanEverTick = true;
	bAlwaysRelevant = true;
	bReplicates = true;
}

void AElementalTeam::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(AElementalTeam, ElementType);
	DOREPLIFETIME(AElementalTeam, ColorRepresentation);
	DOREPLIFETIME(AElementalTeam, TeamPlayers);
}

bool AElementalTeam::AddPlayerToTeam_Validate(AHexPlanetPlayerState* ChosenPlayer)
{
	return true;
}


void AElementalTeam::AddPlayerToTeam_Implementation(AHexPlanetPlayerState* ChosenPlayer)
{
	TeamPlayers.Add(ChosenPlayer);
}

bool AElementalTeam::RemovePlayerFromTeam_Validate(AHexPlanetPlayerState* ChosenPlayer)
{
	return true;
}


void AElementalTeam::RemovePlayerFromTeam_Implementation(AHexPlanetPlayerState* ChosenPlayer)
{
	TeamPlayers.Remove(ChosenPlayer);
}