// Copyright (c) 2018+ True Imagination

#include "PlanetTriangle.h"
#include "Engine/StaticMesh.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "HexPlanetGameState.h"

APlanetTriangle::APlanetTriangle()
{
	PrimaryActorTick.bCanEverTick = true;
	bAlwaysRelevant = true;
	bReplicates = true;
}

void APlanetTriangle::BeginPlay()
{
	Super::BeginPlay();
}

void APlanetTriangle::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(APlanetTriangle, ElementOwner);
	DOREPLIFETIME(APlanetTriangle, ActiveMaterial);
	DOREPLIFETIME(APlanetTriangle, UniqueTag);
}

void APlanetTriangle::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void APlanetTriangle::SetNewMaterial(UMaterialInterface* NewMat)
{
	ActiveMaterial = NewMat;
	MultiCast_SetNewMaterial(NewMat);
}

void APlanetTriangle::OnRep_NewMaterial()
{
	if (GetWorld() == false)
	{
		return;
	}

	AHexPlanetGameState* GS = Cast<AHexPlanetGameState>(GetWorld()->GetGameState());
	if (GS)
	{
		URuntimeMeshComponent* TempMeshComponent = GS->GetTriangleMeshComponent(UniqueTag);
		TempMeshComponent->SetMaterial(0, ActiveMaterial);
	}
}

bool APlanetTriangle::MultiCast_SetNewMaterial_Validate(UMaterialInterface* NewMat) { return true; }
void APlanetTriangle::MultiCast_SetNewMaterial_Implementation(UMaterialInterface* NewMat)
{
	AHexPlanetGameState* GS = Cast<AHexPlanetGameState>(GetWorld()->GetGameState());
	if (GS)
	{
		URuntimeMeshComponent* TempMeshComponent = GS->GetTriangleMeshComponent(UniqueTag);
		TempMeshComponent->SetMaterial(0, ActiveMaterial);
	}
}

void APlanetTriangle::SetNewElementOwer(EElement NewOwner)
{
	AHexPlanetGameState* GS = Cast<AHexPlanetGameState>(GetWorld()->GetGameState());


	if (GS)
	{
		ElementOwner = NewOwner;

		UMaterialInterface* curMaterial = GS->GetTeamMaterial(NewOwner);
		if (curMaterial)
		{
			SetNewMaterial(curMaterial);
		}
	}
}


