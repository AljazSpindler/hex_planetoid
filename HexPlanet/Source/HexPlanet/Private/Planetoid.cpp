// Fill out your copyright notice in the Description page of Project Settings.

#include "Planetoid.h"
#include "HexPlanetGameState.h"
#include "NumericLimits.h"
#include "Engine/World.h"

// Sets default values
APlanetoid::APlanetoid()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	this->SetActorTickInterval(0.5f);

	PlanetRootComp = CreateDefaultSubobject<USceneComponent>(TEXT("PlanetRoot"));
	PlanetRootComp->AttachToComponent(GetRootComponent(), FAttachmentTransformRules::KeepRelativeTransform);
	PlanetRootComp->SetVisibility(true, true);

	AbilityComponent = CreateDefaultSubobject<UPlanetoidAbilityComponent>(TEXT("AbilityComponent"));
}

// This is called when actor is spawned (at runtime or when you drop it into the world in editor)
void APlanetoid::PostActorCreated()
{
	Super::PostActorCreated();
	if (GeneratorType == EPlanetoidGenerator::TRIANGLECLASS1)
	{
		GenerateMeshClass1();
	}
	else if (GeneratorType == EPlanetoidGenerator::TRIANGLECLASS2)
	{
		GenerateMeshClass2();
	}
	else if (GeneratorType == EPlanetoidGenerator::HEXCLASS2)
	{
		GenerateHexMeshClass2();
	}
}

// This is called when actor is already in level and map is opened
void APlanetoid::PostLoad()
{
	Super::PostLoad();
	if (PlanetMeshes.Num() > 0)
	{
		if (PlanetMeshes[0] == nullptr)
		{
			if (GeneratorType == EPlanetoidGenerator::TRIANGLECLASS1)
			{
				GenerateMeshClass1();
			}
			else if (GeneratorType == EPlanetoidGenerator::TRIANGLECLASS2)
			{
				GenerateMeshClass2();
			}
			else if (GeneratorType == EPlanetoidGenerator::HEXCLASS2)
			{
				GenerateHexMeshClass2();
			}
		}
		else
		{
			for (size_t i = 0; i < PlanetMeshes.Num(); i++)
			{
				PlanetMeshes[i]->MeshComponent->RegisterComponentWithWorld(this->GetWorld());
			}
		}
	}
	else
	{
		if (GeneratorType == EPlanetoidGenerator::TRIANGLECLASS1)
		{
			GenerateMeshClass1();
		}
		else if (GeneratorType == EPlanetoidGenerator::TRIANGLECLASS2)
		{
			GenerateMeshClass2();
		}
		else if (GeneratorType == EPlanetoidGenerator::HEXCLASS2)
		{
			GenerateHexMeshClass2();
		}
	}
}

// Called when the game starts or when spawned
void APlanetoid::BeginPlay()
{
	Super::BeginPlay();
	if (GeneratorType == EPlanetoidGenerator::TRIANGLECLASS1)
	{
		GenerateMeshClass1();
	}
	else if (GeneratorType == EPlanetoidGenerator::TRIANGLECLASS2)
	{
		GenerateMeshClass2();
	}
	else if (GeneratorType == EPlanetoidGenerator::HEXCLASS2)
	{
		GenerateHexMeshClass2();
	}
}

// Called every frame
void APlanetoid::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void APlanetoid::ClearProcMeshData()
{
	Vertices.Empty();
	Triangles.Empty();
	Normals.Empty();
	UVS.Empty();
	VertexColors.Empty();
	HexTiles.Empty();
	FaceDual.Empty();

	for (size_t i = 0; i < PlanetMeshes.Num(); i++)
	{
		if (PlanetMeshes[i] != nullptr && PlanetMeshes[i]->MeshComponent != nullptr)
		{
			PlanetMeshes[i]->MeshComponent->UnregisterComponent();
			PlanetMeshes[i]->MeshComponent->DestroyComponent();
			PlanetMeshes[i]->Destroy();
		}
	}

	PlanetMeshes.Empty();
}

#if WITH_EDITOR
void APlanetoid::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{
	Super::PostEditChangeProperty(PropertyChangedEvent);
	FName PropertyName = (PropertyChangedEvent.Property != NULL) ? PropertyChangedEvent.Property->GetFName() : NAME_None;
	if (PropertyName == GET_MEMBER_NAME_CHECKED(APlanetoid, Iterations) || PropertyName == GET_MEMBER_NAME_CHECKED(APlanetoid, ScaleFactor) || PropertyName == GET_MEMBER_NAME_CHECKED(APlanetoid, GeneratorType))
	{
		if (GeneratorType == EPlanetoidGenerator::TRIANGLECLASS1)
		{
			GenerateMeshClass1();
		}
		else if (GeneratorType == EPlanetoidGenerator::TRIANGLECLASS2)
		{
			GenerateMeshClass2();
		}
		else if (GeneratorType == EPlanetoidGenerator::HEXCLASS2)
		{
			GenerateHexMeshClass2();
		}
	}
}
#endif // WITH_EDITOR


// Class 1
void APlanetoid::GenerateMeshClass1()
{
	ClearProcMeshData();

	InitIcosahedronMesh();

	SubdivideClass1(Iterations);

	int i = 0;
	int index = 0;
	while (i < Triangles.Num())
	{
		FTriFace TempTriangle = GenerateTriangleFace(Triangles[i], Triangles[i + 1], Triangles[i + 2], index);
		FaceDual.Add(TempTriangle);

		GenerateMeshComponentFromTriangle(Vertices[Triangles[i]], Vertices[Triangles[i + 1]], Vertices[Triangles[i + 2]], index, &TempTriangle);

		i += 3;
		++index;
	}

	DetermineNeighbours();

	if (GetWorld())
	{
		AHexPlanetGameState* GS = Cast<AHexPlanetGameState>(GetWorld()->GetGameState());

		if (GS)
		{
			GS->ActivePlanet = this;
		}
	}
}

void APlanetoid::InitIcosahedronMesh()
{
	float t = (1.0f + FMath::Sqrt(5.0f)) / 2.0f;

	Vertices.Add(FVector(-1, t, 0).GetSafeNormal() * ScaleFactor);
	Vertices.Add(FVector(1, t, 0).GetSafeNormal() * ScaleFactor);
	Vertices.Add(FVector(-1, -t, 0).GetSafeNormal() * ScaleFactor);
	Vertices.Add(FVector(1, -t, 0).GetSafeNormal() * ScaleFactor);
	Vertices.Add(FVector(0, -1, t).GetSafeNormal() * ScaleFactor);
	Vertices.Add(FVector(0, 1, t).GetSafeNormal() * ScaleFactor);
	Vertices.Add(FVector(0, -1, -t).GetSafeNormal() * ScaleFactor);
	Vertices.Add(FVector(0, 1, -t).GetSafeNormal() * ScaleFactor);
	Vertices.Add(FVector(t, 0, -1).GetSafeNormal() * ScaleFactor);
	Vertices.Add(FVector(t, 0, 1).GetSafeNormal() * ScaleFactor);
	Vertices.Add(FVector(-t, 0, -1).GetSafeNormal() * ScaleFactor);
	Vertices.Add(FVector(-t, 0, 1).GetSafeNormal() * ScaleFactor);

	Triangles.Add(0);  Triangles.Add(11); Triangles.Add(5);
	Triangles.Add(0);  Triangles.Add(5); Triangles.Add(1);
	Triangles.Add(0);  Triangles.Add(1); Triangles.Add(7);
	Triangles.Add(0);  Triangles.Add(7); Triangles.Add(10);
	Triangles.Add(0);  Triangles.Add(10); Triangles.Add(11);
	Triangles.Add(1);  Triangles.Add(5); Triangles.Add(9);
	Triangles.Add(5);  Triangles.Add(11); Triangles.Add(4);
	Triangles.Add(11);  Triangles.Add(10); Triangles.Add(2);
	Triangles.Add(10);  Triangles.Add(7); Triangles.Add(6);
	Triangles.Add(7);  Triangles.Add(1); Triangles.Add(8);
	Triangles.Add(3);  Triangles.Add(9); Triangles.Add(4);
	Triangles.Add(3);  Triangles.Add(4); Triangles.Add(2);
	Triangles.Add(3);  Triangles.Add(2); Triangles.Add(6);
	Triangles.Add(3);  Triangles.Add(6); Triangles.Add(8);
	Triangles.Add(3);  Triangles.Add(8); Triangles.Add(9);
	Triangles.Add(4);  Triangles.Add(9); Triangles.Add(5);
	Triangles.Add(2);  Triangles.Add(4); Triangles.Add(11);
	Triangles.Add(6);  Triangles.Add(2); Triangles.Add(10);
	Triangles.Add(8);  Triangles.Add(6); Triangles.Add(7);
	Triangles.Add(9);  Triangles.Add(8); Triangles.Add(1);

	VertexColors.Add(FLinearColor(1.f, 0.f, 0.f));
	VertexColors.Add(FLinearColor(0.f, 1.f, 0.f));
	VertexColors.Add(FLinearColor(1.f, 0.f, 0.f));
	VertexColors.Add(FLinearColor(0.f, 1.f, 0.f));
	VertexColors.Add(FLinearColor(0.5f, 1.f, 0.5f));
	VertexColors.Add(FLinearColor(0.f, 1.f, 0.f));
	VertexColors.Add(FLinearColor(1.f, 1.f, 0.f));
	VertexColors.Add(FLinearColor(0.f, 1.f, 0.f));
	VertexColors.Add(FLinearColor(1.f, 0.f, 0.f));
	VertexColors.Add(FLinearColor(0.f, 1.f, 0.f));
	VertexColors.Add(FLinearColor(0.f, 1.f, 0.f));
	VertexColors.Add(FLinearColor(1.f, 0.f, 0.f));
}

void APlanetoid::SubdivideClass1(int32 Iter)
{
	TMap<int32, int32> MidPointCache;

	for (int i = 0; i < Iter; i++)
	{
		TArray<int32> NewTriangles;
		int32 j = 0;
		while (j < Triangles.Num())
		{
			// Triangle indices - references Vertices.

			int a = Triangles[j + 0];
			int b = Triangles[j + 1];
			int c = Triangles[j + 2];

			// Create new triangles.

			int ab = GetMidPointIndexClass1(MidPointCache, a, b);
			int bc = GetMidPointIndexClass1(MidPointCache, b, c);
			int ca = GetMidPointIndexClass1(MidPointCache, c, a);

			NewTriangles.Add(a);  NewTriangles.Add(ab); NewTriangles.Add(ca);
			NewTriangles.Add(b);  NewTriangles.Add(bc); NewTriangles.Add(ab);
			NewTriangles.Add(c);  NewTriangles.Add(ca); NewTriangles.Add(bc);
			NewTriangles.Add(ab);  NewTriangles.Add(bc); NewTriangles.Add(ca);

			FLinearColor TriColor = FMath::Lerp(FLinearColor::Green, FLinearColor::Red, FMath::FRandRange(0.0f, 1.0f));

			VertexColors.Add(TriColor);
			VertexColors.Add(TriColor);
			VertexColors.Add(TriColor);

			j += 3;
		}

		Triangles = NewTriangles;
	}
}

int32 APlanetoid::GetMidPointIndexClass1(TMap<int32, int32>& SubCache, int32 PointA, int32 PointB)
{
	int32 smallerIndex = FMath::Min(PointA, PointB);
	int32 greaterIndex = FMath::Max(PointA, PointB);
	int32 key = (smallerIndex << 16) + greaterIndex;

	int32 ret;
	if (SubCache.Contains(key))
		return SubCache[key];

	FVector p1 = Vertices[PointA];
	FVector p2 = Vertices[PointB];
	FVector middle = FMath::Lerp(p1, p2, 0.5f).GetSafeNormal();

	ret = Vertices.Num();
	Vertices.Add(middle * ScaleFactor);

	SubCache.Add(key, ret);
	return ret;
}

void APlanetoid::GenerateMeshComponentFromTriangle(FVector Vertex1, FVector Vertex2, FVector Vertex3, int32 UID, FTriFace* Face)
{
	TArray<FVector> TempVertices = { Vertex1, Vertex2, Vertex3 };
	TArray<FVector> TempNormals;

	float DistFromCoreToSurface = FVector::Dist(FVector(0), Face->Centroid);
	FVector SurfaceAndNormal = Face->Centroid + Face->Normal;
	float DistFromCoreToNormal = FVector::Dist(FVector(0), SurfaceAndNormal);

	TArray<int32> TempTriangles;

	if (DistFromCoreToNormal < DistFromCoreToSurface)
	{
		TempTriangles = { 0, 1, 2 };
		TempNormals = { -Face->Normal, -Face->Normal, -Face->Normal };
	}
	else
	{
		TempTriangles = { 0, 2, 1 };
		TempNormals = { Face->Normal, Face->Normal, Face->Normal };
	}
	// TODO: Fill in the Tangents and UVs if available.
	TArray<FVector2D> TempUvs;
	TArray<FRuntimeMeshTangent> TempTangents;

	FLinearColor TriColor = FMath::Lerp(FLinearColor::Green, FLinearColor::Red, FMath::FRandRange(0.0f, 1.0f));
	FColor TempColor = TriColor.ToFColor(false);
	TArray<FColor> TempColors;
	TempColors.Add(TempColor); TempColors.Add(TempColor); TempColors.Add(TempColor);

	//spawn actor that holds data for the mesh component
	FActorSpawnParameters SpawnParams;
	SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	SpawnParams.Owner = this;

	if (GetWorld())
	{
		APlanetTriangle* curTriangle = GetWorld()->SpawnActor<APlanetTriangle>(APlanetTriangle::StaticClass(), SpawnParams);
		if (curTriangle)
		{

			URuntimeMeshComponent* TempComp;
			TempComp = NewObject<URuntimeMeshComponent>(curTriangle, URuntimeMeshComponent::StaticClass());
			TempComp->AttachToComponent(PlanetRootComp, FAttachmentTransformRules::KeepRelativeTransform);
			TempComp->RegisterComponentWithWorld(this->GetWorld());
			TempComp->SetIsReplicated(true);

			TempComp->CreateMeshSection(0, TempVertices, TempTriangles, TempNormals, TempUvs, TempColors, TempTangents, true, EUpdateFrequency::Infrequent);
			TempComp->SetVisibility(true);

			curTriangle->MeshComponent = TempComp;
			curTriangle->UniqueTag = UID;
			curTriangle->SetNewElementOwer(EElement::NEUTRAL);

			PlanetMeshes.Add(curTriangle);
		}
	}
}

FTriFace APlanetoid::GenerateTriangleFace(int32 Index1, int32 Index2, int32 Index3, int32 UID)
{
	FTriFace TempTriangle = FTriFace(Index1, Index2, Index3);
	TempTriangle.Centroid = TempTriangle.GetCentroidClass1(Vertices);
	TempTriangle.Normal = TempTriangle.GetNormalClass1(Vertices);
	TempTriangle.UID = UID;
	if (GetRootComponent() != nullptr)
	{
		TempTriangle.Rotation = TempTriangle.GetRotationClass1(GetRootComponent());
	}

	return TempTriangle;
}

// Class 2
void APlanetoid::GenerateMeshClass2()
{
	ClearProcMeshData();

	InitIcosahedronMesh();

	for (FVector Vert : Vertices)
	{
		HexTiles.Add(FHexTile(Vert));
	}

	int32 i = 0;
	int32 index = 0;
	while (i < Triangles.Num())
	{
		FaceDual.Add(FTriFace(Triangles[i], Triangles[i + 1], Triangles[i + 2]));
		FaceDual[index].UID = index;
		i += 3;
		++index;
	}

	FindNeighboursClass2();

	SubdivideClass2(Iterations);

	index = 0;
	Triangles.Empty();
	i = 0;
	while (i < FaceDual.Num())
	{
			Triangles.Add(FaceDual[i].PointA);
			Triangles.Add(FaceDual[i].PointB);
			Triangles.Add(FaceDual[i].PointC);

			GenerateMeshComponentFromTriangle(Vertices[FaceDual[i].PointA], Vertices[FaceDual[i].PointB], Vertices[FaceDual[i].PointC], i, &FaceDual[i]);
			++i;
	}

	if (GetWorld())
	{
		AHexPlanetGameState* GS = Cast<AHexPlanetGameState>(GetWorld()->GetGameState());

		if (GS)
		{
			GS->ActivePlanet = this;
		}
	}
}

void APlanetoid::SubdivideClass2(int32 Iter)
{
	TArray<TPair<int32, int32>> EdgeDone;
	TArray<FTriFace> newHexDuals;

	int ik = 0;
	for (int i = 0; i < Iter; i++)
	{
		int32 j = 0;
		while (j < FaceDual.Num())
		{
			// Create a new vertex in the center of the triangle since this will be our new hex.
			FaceDual[j].NewVert = HexTiles.Num();
			FVector newHexCenter = FaceDual[j].GetCentroidClass1(Vertices);
			FaceDual[j].Centroid = newHexCenter;
			FaceDual[j].Normal = FaceDual[j].GetNormalClass1(Vertices);
			HexTiles.Add(FHexTile(newHexCenter));
			HexTiles.Last().Element = (EElement)FMath::RandRange(0, 3);
			HexTiles.Last().UID = HexTiles.Num() - 1;

			Vertices.Add(newHexCenter.GetSafeNormal() * ScaleFactor);

			j++;
		}

		j = 0;
		while (j < FaceDual.Num())
		{
			// Triangle indices - references Vertices.

			FTriFace &t = FaceDual[j];

			if (t.ABNeighbour > -1)
			{
				if (CreateTrisFromEdgeClass2(EdgeDone, newHexDuals, t.UID, t.ABNeighbour, t.PointA, t.PointB))
				{
					newHexDuals[ik].UID = ik;
					newHexDuals[ik].Centroid = newHexDuals[ik].GetCentroidClass1(Vertices);
					newHexDuals[ik].Normal = newHexDuals[ik].GetNormalClass1(Vertices);
					ik++;
					newHexDuals[ik].UID = ik;
					newHexDuals[ik].Centroid = newHexDuals[ik].GetCentroidClass1(Vertices);
					newHexDuals[ik].Normal = newHexDuals[ik].GetNormalClass1(Vertices);
					ik++;
				}
			}

			if (t.BCNeighbour > -1)
			{
				if (CreateTrisFromEdgeClass2(EdgeDone, newHexDuals, t.UID, t.BCNeighbour, t.PointB, t.PointC))
				{
					newHexDuals[ik].UID = ik;
					newHexDuals[ik].Centroid = newHexDuals[ik].GetCentroidClass1(Vertices);
					newHexDuals[ik].Normal = newHexDuals[ik].GetNormalClass1(Vertices);
					ik++;
					newHexDuals[ik].UID = ik;
					newHexDuals[ik].Centroid = newHexDuals[ik].GetCentroidClass1(Vertices);
					newHexDuals[ik].Normal = newHexDuals[ik].GetNormalClass1(Vertices);
					ik++;
				}
			}

			if (t.CANeighbour > -1)
			{
				if (CreateTrisFromEdgeClass2(EdgeDone, newHexDuals, t.UID, t.CANeighbour, t.PointC, t.PointA))
				{
					newHexDuals[ik].UID = ik;
					newHexDuals[ik].Centroid = newHexDuals[ik].GetCentroidClass1(Vertices);
					newHexDuals[ik].Normal = newHexDuals[ik].GetNormalClass1(Vertices);
					ik++;
					newHexDuals[ik].UID = ik;
					newHexDuals[ik].Centroid = newHexDuals[ik].GetCentroidClass1(Vertices);
					newHexDuals[ik].Normal = newHexDuals[ik].GetNormalClass1(Vertices);
					ik++;
				}
			}

			FLinearColor TriColor = FMath::Lerp(FLinearColor::Green, FLinearColor::Red, FMath::FRandRange(0.0f, 1.0f));

			VertexColors.Add(TriColor);
			VertexColors.Add(TriColor);
			VertexColors.Add(TriColor);

			j++;
		}

		FaceDual = newHexDuals;	
		newHexDuals.Empty();
		ik = 0;

		FindNeighboursClass2();

	}
}

bool APlanetoid::CreateTrisFromEdgeClass2(TArray<TPair<int32, int32>> &EdgeDone, TArray<FTriFace> &TriList, int32 Tri, int32 OtherTri, int32 EdgeA, int32 EdgeB)
{
	TPair<int32, int32> edgeId(FMath::Min(EdgeA, EdgeB), FMath::Max(EdgeA, EdgeB));

	if (EdgeDone.Find(edgeId) == INDEX_NONE)
	{
		TriList.Add(FTriFace(EdgeA, FaceDual[Tri].NewVert, FaceDual[OtherTri].NewVert));
		TriList.Add(FTriFace(FaceDual[Tri].NewVert, FaceDual[OtherTri].NewVert, EdgeB));
		EdgeDone.Add(edgeId);

		return true;
	}

	return false;
}

void APlanetoid::FindNeighboursClass2()
{
	for (size_t i = 0; i < HexTiles.Num(); i++)
	{
		HexTiles[i].HexTris.Empty();
		HexTiles[i].HexVertices.Empty();
		HexTiles[i].CenterVertexIndex = -1;
	}

	for (size_t i = 0; i < FaceDual.Num(); i++)
	{
		for (size_t j = 0; j < FaceDual.Num(); j++)
		{
			if (i == j)
			{
				continue;
			}

			if (EdgeMatchClass2(FaceDual[i].PointA, FaceDual[i].PointB, FaceDual[j].PointA, FaceDual[j].PointB, FaceDual[j].PointC))
			{
				FaceDual[i].ABNeighbour = FaceDual[j].UID;
			}

			if (EdgeMatchClass2(FaceDual[i].PointB, FaceDual[i].PointC, FaceDual[j].PointA, FaceDual[j].PointB, FaceDual[j].PointC))
			{
				FaceDual[i].BCNeighbour = FaceDual[j].UID;
			}

			if (EdgeMatchClass2(FaceDual[i].PointC, FaceDual[i].PointA, FaceDual[j].PointA, FaceDual[j].PointB, FaceDual[j].PointC))
			{
				FaceDual[i].CANeighbour = FaceDual[j].UID;
			}
		}

		HexTiles[FaceDual[i].PointA].HexTris.Add(FaceDual[i].UID);
		HexTiles[FaceDual[i].PointB].HexTris.Add(FaceDual[i].UID);
		HexTiles[FaceDual[i].PointC].HexTris.Add(FaceDual[i].UID);
	}

	TArray<FTriFace> FaceTemp = FaceDual;

	for (size_t i = 0; i < HexTiles.Num(); i++)
	{
		TArray<int32> TempTileVertIndexes;
		for (size_t j = 0; j < HexTiles[i].HexTris.Num(); j++)
		{
			FVector V1 = FaceDual[HexTiles[i].HexTris.Num() - 1].GetCenterClass2(HexTiles) - HexTiles[i].Position;
			FVector Norm = FaceDual[HexTiles[i].HexTris[j]].GetCenterClass2(HexTiles);
			FVector V2 = Norm - HexTiles[i].Position;

			Norm = Norm.GetSafeNormal();
			V1 = V1.GetSafeNormal();
			V2 = V2.GetSafeNormal();

			float angle = FMath::Acos(FVector::DotProduct(V1, V2));
			float dir = FVector::DotProduct(Norm, FVector::CrossProduct(V1, V2));
			if (dir < 0.0f)
			{
				angle = PI + (PI - angle);
			}
			FaceDual[HexTiles[i].HexTris[j]].Angle = angle;

			TempTileVertIndexes.Add(FaceDual[HexTiles[i].HexTris[j]].PointA);
			TempTileVertIndexes.Add(FaceDual[HexTiles[i].HexTris[j]].PointB);
			TempTileVertIndexes.Add(FaceDual[HexTiles[i].HexTris[j]].PointC);

			HexTiles[i].HexVertices.AddUnique(FaceDual[HexTiles[i].HexTris[j]].PointA);
			HexTiles[i].HexVertices.AddUnique(FaceDual[HexTiles[i].HexTris[j]].PointB);
			HexTiles[i].HexVertices.AddUnique(FaceDual[HexTiles[i].HexTris[j]].PointC);
		}

		TempTileVertIndexes.SetNum(6, true); // We actually need the first 2 triangles.

		for (int32 Ind : TempTileVertIndexes)
		{
			int32 Cnt = 0;
			for (int32 Ind2 : TempTileVertIndexes)
			{
				if (Ind == Ind2)
				{
					++Cnt;
				}

				if (Cnt > 1)
				{
					HexTiles[i].CenterVertexIndex = Ind;
					break;
				}
			}
		}

		HexTiles[i].HexTris.Sort([&, FaceTemp](int32 A, int32 B) { return FaceTemp[A].Angle < FaceTemp[B].Angle; });
	}
}

bool APlanetoid::EdgeMatchClass2(int32 A, int32 B, int32 OtherA, int32 OtherB, int32 OtherC)
{
	if (((A == OtherA) && (B == OtherB)) || ((A == OtherB) && (B == OtherC)) ||
		((A == OtherC) && (B == OtherA)) || ((B == OtherA) && (A == OtherB)) ||
		((B == OtherB) && (A == OtherC)) || ((B == OtherC) && (A == OtherA)))
	{
		return true;
	}

	return false;
}

void APlanetoid::DetermineNeighbours()
{
	for (FTriFace& Face : FaceDual)
	{
		for (FTriFace OtherFace : FaceDual)
		{
			if (Face.UID == OtherFace.UID)
			{
				continue;
			}

			if (Face.IsEdgeNeighbourOf(OtherFace))
			{
				Face.EdgeNeighbours.Add(OtherFace.UID);
			}

			if (Face.IsVertNeighbourOf(OtherFace))
			{
				if (!Face.VertNeighbours.Contains(OtherFace.UID))
				{
					Face.VertNeighbours.Add(OtherFace.UID);
				}
			}
		}
		UE_LOG(LogTemp, Warning, TEXT("Face %i has %i edge neighbours."), Face.UID, Face.EdgeNeighbours.Num());
		UE_LOG(LogTemp, Warning, TEXT("Face %i has %i vert neighbours."), Face.UID, Face.VertNeighbours.Num());
	}
}

void APlanetoid::BuildTiles()
{
	TArray<FHexTile> NewTiles;
	TArray<FHexTile> PentTiles;
	TArray<FHexTile> OtherTiles;
	TArray<FHexTile> LastRing;

	// First we take out the Pents.
	for (FHexTile Tile : HexTiles)
	{
		if (Tile.HexTris.Num() == 5)
		{
			PentTiles.Add(Tile);
			NewTiles.Add(Tile);
		}
		else
		{
			OtherTiles.Add(Tile);
		}
	}

	TArray<FHexTile> AlreadyVisited;
	for (FHexTile Tile : PentTiles)
	{
		int32 NeighbourCnt = 0;
		TMap<int32, float> Distances;
		int ik = 0;
		for (FHexTile OtherTile : OtherTiles)
		{
			/*if (Tile.IsTileNeighbourOf(OtherTile) && NeighbourCnt < 5)
			{
				NewTiles.AddUnique(OtherTile);
				NeighbourCnt++;
				AlreadyVisited.AddUnique(OtherTile);
			}
			if (Tile.IsContainingTile(OtherTile))
			{
				AlreadyVisited.AddUnique(OtherTile);
			}*/
			FVector P1 = Tile.Position.GetSafeNormal();
			FVector P2 = OtherTile.Position.GetSafeNormal();

			float AngleRad = FMath::Acos(FVector::DotProduct(P1, P2));
			float Radius = FVector::Dist(FVector(0, 0, 0), Tile.Position);

			Distances.Add(ik, (AngleRad * Radius));
			++ik;
		}

		Distances.ValueSort([](float A, float B) {
			return A < B;
		});

		int32 SkipCnt = 5;
		for (auto& Element : Distances)
		{
			if (SkipCnt < 1)
			{
				NewTiles.AddUnique(OtherTiles[Element.Key]);
				LastRing.AddUnique(OtherTiles[Element.Key]);
				for (int32 TempInd : OtherTiles[Element.Key].HexVertices)
				{
					AlreadyVisited.AddUnique(HexTiles[TempInd]);
				}
				++NeighbourCnt;
			}
			--SkipCnt;
			if (NeighbourCnt == 5)
			{
				break;
			}
		}
	}

	for (FHexTile AVisited : AlreadyVisited)
	{
		OtherTiles.Remove(AVisited);
	}
	/*
	if (OtherTiles.Num() > 0)
	{
		TArray<FHexTile> TempTiles = OtherTiles;
		//while (TempTiles.Num() > 0)
		//{
			AlreadyVisited.Empty();
			int32 NeighbourCnt = 0;
			TMap<int32, float> Distances;
			int ik = 0;
			for (FHexTile Tile : LastRing)
			{
				for (FHexTile OtherTile : HexTiles)
				{

					FVector P1 = Tile.Position.GetSafeNormal();
					FVector P2 = OtherTile.Position.GetSafeNormal();

					float AngleRad = FMath::Acos(FVector::DotProduct(P1, P2));
					float Radius = FVector::Dist(FVector(0, 0, 0), Tile.Position);

					Distances.Add(ik, (AngleRad * Radius));
					++ik;
				}

				Distances.ValueSort([](float A, float B) {
					return A < B;
				});

				int32 SkipCnt = 6;
				for (auto& Element : Distances)
				{
					if (SkipCnt < 1)
					{
						NewTiles.AddUnique(HexTiles[Element.Key]);
						for (int32 TempInd : HexTiles[Element.Key].HexVertices)
						{
							AlreadyVisited.AddUnique(HexTiles[TempInd]);
						}
						++NeighbourCnt;
					}
					--SkipCnt;
					if (NeighbourCnt == 6)
					{
						break;
					}
				}
			}

			for (FHexTile AVisited : AlreadyVisited)
			{
				OtherTiles.Remove(AVisited);
			}

		//}
	}
	*/
	HexTiles = NewTiles;
}

void APlanetoid::GenerateHexMeshClass2()
{
	ClearProcMeshData();

	InitIcosahedronMesh();

	int32 i = 0;
	for (FVector Vert : Vertices)
	{
		HexTiles.Add(FHexTile(Vert));
		HexTiles[i].UID = i;
		++i;
	}

	i = 0;
	int32 index = 0;
	while (i < Triangles.Num())
	{
		FaceDual.Add(FTriFace(Triangles[i], Triangles[i + 1], Triangles[i + 2]));
		FaceDual[index].UID = index;
		i += 3;
		++index;
	}

	FindNeighboursClass2();

	SubdivideClass2(Iterations);

	index = 0;
	Triangles.Empty();
	i = 0;

	BuildTiles();

	while (i < HexTiles.Num())
	{
		HexTiles[i].UID = i;
		GenerateMeshComponentFromHex(HexTiles[i], i);
		++i;
	}

	if (GetWorld())
	{
		AHexPlanetGameState* GS = Cast<AHexPlanetGameState>(GetWorld()->GetGameState());

		if (GS)
		{
			GS->ActivePlanet = this;
		}
	}
}


void APlanetoid::GenerateMeshComponentFromHex(FHexTile Tile, int32 UID)
{
	TArray<FVector> TempVertices;
	TArray<int32> TempTriangles;
	TArray<FVector> TempNormals;
	// TODO: Fill in the Tangents and UVs if available.

	int32 TriIndex = 0;
	for (int32 i = 0; i < Tile.HexTris.Num(); i++)
	{
		float DistFromCoreToSurface = FVector::Dist(FVector(0), FaceDual[Tile.HexTris[i]].Centroid);
		FVector SurfaceAndNormal = FaceDual[Tile.HexTris[i]].Centroid + FaceDual[Tile.HexTris[i]].Normal;
		float DistFromCoreToNormal = FVector::Dist(FVector(0), SurfaceAndNormal);

		TArray<int32> WindingIndexes;

		int32 IndexOfVertA = TempVertices.IndexOfByKey(Vertices[FaceDual[Tile.HexTris[i]].PointA]);
		if (IndexOfVertA < 0)
		{
			TempVertices.Add(Vertices[FaceDual[Tile.HexTris[i]].PointA]);
			WindingIndexes.Add(TriIndex);
			++TriIndex;
		}
		else
		{
			WindingIndexes.Add(IndexOfVertA);
		}

		int32 IndexOfVertB = TempVertices.IndexOfByKey(Vertices[FaceDual[Tile.HexTris[i]].PointB]);
		if (IndexOfVertB < 0)
		{
			TempVertices.Add(Vertices[FaceDual[Tile.HexTris[i]].PointB]);
			WindingIndexes.Add(TriIndex);
			++TriIndex;
		}
		else
		{
			WindingIndexes.Add(IndexOfVertB);
		}

		int32 IndexOfVertC = TempVertices.IndexOfByKey(Vertices[FaceDual[Tile.HexTris[i]].PointC]);
		if (IndexOfVertC < 0)
		{
			TempVertices.Add(Vertices[FaceDual[Tile.HexTris[i]].PointC]);
			WindingIndexes.Add(TriIndex);
			++TriIndex;
		}
		else
		{
			WindingIndexes.Add(IndexOfVertC);
		}

		if (DistFromCoreToNormal < DistFromCoreToSurface)
		{
			TempTriangles.Add(WindingIndexes[0]);
			TempTriangles.Add(WindingIndexes[1]);
			TempTriangles.Add(WindingIndexes[2]);

			TempNormals.Add(-FaceDual[Tile.HexTris[i]].Normal);
			TempNormals.Add(-FaceDual[Tile.HexTris[i]].Normal);
			TempNormals.Add(-FaceDual[Tile.HexTris[i]].Normal);
		}
		else
		{
			TempTriangles.Add(WindingIndexes[0]);
			TempTriangles.Add(WindingIndexes[2]);
			TempTriangles.Add(WindingIndexes[1]);

			TempNormals.Add(FaceDual[Tile.HexTris[i]].Normal);
			TempNormals.Add(FaceDual[Tile.HexTris[i]].Normal);
			TempNormals.Add(FaceDual[Tile.HexTris[i]].Normal);
		}
	}

	if (TempVertices.Num() > 1)
	{
		TArray<FVector2D> TempUvs;
		TArray<FRuntimeMeshTangent> TempTangents;

		FLinearColor TriColor = FMath::Lerp(FLinearColor::Green, FLinearColor::Red, FMath::FRandRange(0.0f, 1.0f));
		FColor TempColor = TriColor.ToFColor(false);
		TArray<FColor> TempColors;
		TempColors.Add(TempColor); TempColors.Add(TempColor); TempColors.Add(TempColor);

		//spawn actor that holds data for the mesh component
		FActorSpawnParameters SpawnParams;
		SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		SpawnParams.Owner = this;

		if (GetWorld())
		{
			APlanetTriangle* curTriangle = GetWorld()->SpawnActor<APlanetTriangle>(APlanetTriangle::StaticClass(), SpawnParams);
			if (curTriangle)
			{

				URuntimeMeshComponent* TempComp;
				TempComp = NewObject<URuntimeMeshComponent>(curTriangle, URuntimeMeshComponent::StaticClass());
				TempComp->AttachToComponent(PlanetRootComp, FAttachmentTransformRules::KeepRelativeTransform);
				TempComp->RegisterComponentWithWorld(this->GetWorld());
				TempComp->SetIsReplicated(true);

				TempComp->CreateMeshSection(0, TempVertices, TempTriangles, TempNormals, TempUvs, TempColors, TempTangents, true, EUpdateFrequency::Infrequent);
				TempComp->SetVisibility(true);

				curTriangle->MeshComponent = TempComp;
				curTriangle->UniqueTag = UID;
				curTriangle->SetNewElementOwer(EElement::NEUTRAL);

				PlanetMeshes.Add(curTriangle);
			}
		}
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("Could not construct hex/pent!"));
	}
}