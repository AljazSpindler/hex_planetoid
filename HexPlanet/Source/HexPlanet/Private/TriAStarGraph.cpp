// Copyright (c) 2018+ True Imagination

#include "TriAStarGraph.h"

int32 TriAStarGraph::GetNeighbourCount(const FNodeRef NodeRef) const
{
	TArray<FTriFace> InGrid = *Grid;
	return InGrid[NodeRef].VertNeighbours.Num();
}

bool TriAStarGraph::IsValidRef(const FNodeRef NodeRef) const
{
	// We assume a node is always valid.
	return true;
}

TriAStarGraph::FNodeRef TriAStarGraph::GetNeighbour(const FNodeRef NodeRef, const int32 NeighbourIndex) const
{
	TArray<FTriFace> InGrid = *Grid;
	return InGrid[NodeRef].VertNeighbours[NeighbourIndex];
}
