// Copyright (c) 2018+ True Imagination

#include "PlanetoidAbilityComponent.h"
#include "TriAStarGraph.h"


// Sets default values for this component's properties
UPlanetoidAbilityComponent::UPlanetoidAbilityComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	// ...
}


// Called when the game starts
void UPlanetoidAbilityComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UPlanetoidAbilityComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UPlanetoidAbilityComponent::AStarPathfinding(int32 StartPoint, int32 EndPoint, TArray<FTriFace> Grid, TMap<FTriFace, float> Cost, TSet<FTriFace> Blocked, bool& bSuccess, TArray<int32>& Results)
{
	TArray<FTriFace> InGrid = Grid;

	struct FAStarHexFilter
	{
		const bool bAllowInexact;
		const TriAStarGraph& Framework;

		explicit FAStarHexFilter(const bool bExactSearch, const TriAStarGraph& InFramework)
			: bAllowInexact(bExactSearch), Framework(InFramework)
		{
		}

		FORCEINLINE float GetHeuristicScale() const
		{
			return 1.f;
		}
		FORCEINLINE float GetHeuristicCost(const TriAStarGraph::FNodeRef StartNodeRef, const TriAStarGraph::FNodeRef EndNodeRef) const
		{
			// We need great circle distance here.
			return CalculateGreatCircleDistance(StartNodeRef, EndNodeRef);
		}

		FORCEINLINE float CalculateGreatCircleDistance(const TriAStarGraph::FNodeRef StartNodeRef, const TriAStarGraph::FNodeRef EndNodeRef) const
		{
			TArray<FTriFace> TempGrid = *Framework.Grid;
			FVector P1 = TempGrid[StartNodeRef].Centroid.GetSafeNormal();
			FVector P2 = TempGrid[EndNodeRef].Centroid.GetSafeNormal();

			float AngleRad = FMath::Acos(FVector::DotProduct(P1, P2));
			float Radius = FVector::Dist(FVector(0, 0, 0), TempGrid[StartNodeRef].Centroid);

			return AngleRad * Radius;
		}

		FORCEINLINE float GetTraversalCost(const TriAStarGraph::FNodeRef StartNodeRef, const TriAStarGraph::FNodeRef EndNodeRef) const
		{
			TArray<FTriFace> InGrid = *Framework.Grid;
			const float* TileCostPointer = Framework.Cost->Find(InGrid[EndNodeRef]);
			float TileCost = (TileCostPointer) ? *TileCostPointer : 1.f;
			// We need great circle distance here.
			return TileCost * CalculateGreatCircleDistance(StartNodeRef, EndNodeRef); // Multiply by Tile Cost
		}
		FORCEINLINE bool IsTraversalAllowed(const TriAStarGraph::FNodeRef NodeA, const TriAStarGraph::FNodeRef NodeB) const
		{
			return true;
		}
		FORCEINLINE bool WantsPartialSolution() const
		{
			return bAllowInexact;
		}
	};

	TriAStarGraph GridWrapper(&Cost, &InGrid, &Blocked);

	FGraphAStar<TriAStarGraph> PathFinder(GridWrapper);
	FAStarHexFilter HexFilter(bAllowInexact, GridWrapper);
	const EGraphAStarResult Status = PathFinder.FindPath(StartPoint, EndPoint, HexFilter, Results);
	bSuccess = (Status == EGraphAStarResult::SearchSuccess);
}

void UPlanetoidAbilityComponent::EdgeRing(int32 StartPoint, bool ExcludeStartingPoint, int32 Radius, TArray<FTriFace> Grid, TSet<FTriFace> Blocked, bool& bSuccess, TArray<int32>& Results)
{
	if (Radius < 1)
	{
		bSuccess = false;
		return;
	}

	bSuccess = true;
	int32 StartingPoint = StartPoint;
	for (int32 EdgeNeighbour : Grid[StartingPoint].EdgeNeighbours)
	{
		Results.Add(EdgeNeighbour);
	}

	int ArrayIndex = 0;
	int32 LoopIt = 1;
	while (LoopIt < Radius)
	{
		TArray<int32> TempResults;
		for (int32 i = ArrayIndex; i < Results.Num(); i++)
		{
			TempResults.Add(Results[i]);
		}

		ArrayIndex = TempResults.Num();

		for (int32 EdgeNeighbour : TempResults)
		{
			StartingPoint = EdgeNeighbour;
			for (int32 NextEdgeNeighbour : Grid[StartingPoint].EdgeNeighbours)
			{
				if (!Results.Contains(NextEdgeNeighbour))
				{
					Results.Add(NextEdgeNeighbour);
				}
			}
		}

		LoopIt++;
	}

	if (ExcludeStartingPoint)
	{
		Results.Remove(StartPoint);
	}
}

void UPlanetoidAbilityComponent::PointRing(int32 StartPoint, bool ExcludeStartingPoint, int32 Radius, TArray<FTriFace> Grid, TSet<FTriFace> Blocked, bool& bSuccess, TArray<int32>& Results)
{
	if (Radius < 1)
	{
		bSuccess = false;
		return;
	}

	bSuccess = true;
	int32 StartingPoint = StartPoint;
	for (int32 VertNeighbour : Grid[StartingPoint].VertNeighbours)
	{
		Results.Add(VertNeighbour);
	}

	int ArrayIndex = 0;
	int32 LoopIt = 1;
	while (LoopIt < Radius)
	{
		TArray<int32> TempResults;
		for (int32 i = ArrayIndex; i < Results.Num(); i++)
		{
			TempResults.Add(Results[i]);
		}

		ArrayIndex = TempResults.Num();

		for (int32 VertNeighbour : TempResults)
		{
			StartingPoint = VertNeighbour;
			for (int32 NextVertNeighbour : Grid[StartingPoint].VertNeighbours)
			{
				if (!Results.Contains(NextVertNeighbour))
				{
					Results.Add(NextVertNeighbour);
				}
			}
		}

		LoopIt++;
	}

	if (ExcludeStartingPoint)
	{
		Results.Remove(StartPoint);
	}
}

void UPlanetoidAbilityComponent::PiercePlanet(int32 StartPoint, TArray<FTriFace> Grid, TSet<FTriFace> Blocked, bool& bSuccess, int32& Result)
{
	// TODO: Think if we should we rather use the dot or cross product to get the actual angle here. We need to check performance. Since this method now can be error prone if the radius is small.
	float Diameter = 2 * FVector::Dist(FVector(0, 0, 0), Grid[StartPoint].Centroid);

	for (int32 i = 0; i < Grid.Num(); i++)
	{
		if (i == StartPoint)
		{
			continue;
		}

		if (FMath::Abs((Diameter - FVector::Dist(Grid[StartPoint].Centroid, Grid[i].Centroid))) < KINDA_SMALL_NUMBER)
		{
			bSuccess = true;
			Result = i;
			break;
		}

	}
}

