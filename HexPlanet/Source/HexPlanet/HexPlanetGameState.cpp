// Copyright (c) 2018+ True Imagination

#include "HexPlanetGameState.h"
#include "Engine/World.h"
#include "Kismet/GameplayStatics.h"
#include "Gameplay/PlanetTriangle.h"
#include "HexPlanetCharacter.h"
#include "HexPlanetPlayerController.h"
#include "HexPlanetDataTables.h"
#include "Planetoid.h"

AHexPlanetGameState::AHexPlanetGameState()
{
	bAlwaysRelevant = true;
	bReplicates = true;
}

void AHexPlanetGameState::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(AHexPlanetGameState, Teams);
	DOREPLIFETIME(AHexPlanetGameState, Abilities);
	DOREPLIFETIME(AHexPlanetGameState, TurnBased_GameStatus); 
	DOREPLIFETIME(AHexPlanetGameState, TeamScores);
}

FTeamScore AHexPlanetGameState::GetTeamScore(EElement ElementType, int &Index)
{
	for (int i = 0; i < TeamScores.Num(); i++)
	{
		if (ElementType == TeamScores[i].TeamElement)
		{
			Index = i;
			return TeamScores[i];
		}
	}
	return FTeamScore();
}

bool AHexPlanetGameState::CreateTeam_Validate(EElement ChosenElementType, FColor ChosenColor)
{
	return true;
}

void AHexPlanetGameState::CreateTeam_Implementation(EElement ChosenElementType, FColor ChosenColor)
{
	FVector Location(0.0f, 0.0f, -500.0f);
	FRotator Rotation(0.0f, 0.0f, 0.0f);
	FActorSpawnParameters SpawnInfo;
	SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	AElementalTeam* curTeam = GetWorld()->SpawnActor<AElementalTeam>(Location, Rotation, SpawnInfo);

	curTeam->ElementType = ChosenElementType;
	curTeam->ColorRepresentation = ChosenColor;
	curTeam->TeamMaterialReference = GetTeamMaterial(curTeam->ElementType);
	Teams.Add(curTeam);

	FTeamScore curTeamScore;
	curTeamScore.TeamElement = curTeam->ElementType;
	curTeamScore.TotalScore = 0;
	TeamScores.Add(curTeamScore);
}

void AHexPlanetGameState::BeginPlay()
{
	Super::BeginPlay();

	if (Role == ROLE_Authority)
	{
		CreateTeam(EElement::Fire, FColor::Red);
		CreateTeam(EElement::Water, FColor::Blue);
		CreateTeam(EElement::Earth, FColor::FromHex("#785543"));
		CreateTeam(EElement::Wind, FColor::Silver);
		SpawnAbilities();
	}
}

bool AHexPlanetGameState::RemovePlayerFromTeam_Validate(EElement ChosenElementType, AHexPlanetPlayerState* ChosenPlayer)
{
	return true;
}

void AHexPlanetGameState::RemovePlayerFromTeam_Implementation(EElement ChosenElementType, AHexPlanetPlayerState* ChosenPlayer)
{
	AElementalTeam* ChosenTeam = FindTeam(ChosenElementType);

	if (ChosenTeam)
	{
		ChosenTeam->RemovePlayerFromTeam(ChosenPlayer);
	}
}

bool AHexPlanetGameState::ChangePlayerTeam_Validate(EElement ChosenElementType, AHexPlanetPlayerState* ChosenPlayer)
{
	return true;
}

void AHexPlanetGameState::ChangePlayerTeam_Implementation(EElement ChosenElementType, AHexPlanetPlayerState* ChosenPlayer)
{
	bool bDone = false;

	for (int i = 0; i < Teams.Num(); i++)
	{
		TArray<AHexPlanetPlayerState*> Players = Teams[i]->GetTeamPlayers();
		for (int j = 0; j < Players.Num(); j++)
		{
			if (Players[j] == ChosenPlayer)
			{
				AddPlayerToTeam(ChosenElementType, Players[j]);
				RemovePlayerFromTeam(Teams[i]->ElementType, Players[j]);
				bDone = true;
				break;
			}
		}

		if (bDone)
		{
			break;
		}
	}
}

TArray<APlanetTriangle*> AHexPlanetGameState::GetAllTeamOwnedMeshes(EElement TeamElement)
{
	TArray<APlanetTriangle*> TeamMeshes;

	for (int i = 0; ActivePlanet->PlanetMeshes.Num(); i++)
	{
		if (ActivePlanet->PlanetMeshes[i]->ElementOwner == TeamElement)
		{
			TeamMeshes.Add(ActivePlanet->PlanetMeshes[i]);
		}
	}

	return TeamMeshes;
}

bool AHexPlanetGameState::AddPlayerToTeam_Validate(EElement ChosenElementType, AHexPlanetPlayerState* ChosenPlayer)
{
	return true;
}

void AHexPlanetGameState::AddPlayerToTeam_Implementation(EElement ChosenElementType, AHexPlanetPlayerState* ChosenPlayer)
{
	if (ChosenElementType != EElement::MAX && ChosenPlayer)
	{
		AElementalTeam* FoundTeam = FindTeam(ChosenElementType);
		if (FoundTeam)
		{
			FoundTeam->AddPlayerToTeam(ChosenPlayer);
		}
	}
}

AElementalTeam* AHexPlanetGameState::FindTeam(EElement ElementType)
{
	for (int i = 0; i < Teams.Num(); i++)
	{
		if (Teams[i])
		{
			if (Teams[i]->ElementType == ElementType)
			{
				return Teams[i];
			}
		}
	}

	return nullptr;
}


UMaterialInterface* AHexPlanetGameState::GetTeamMaterial(EElement TeamType)
{
	if (TeamMaterialDatabase)
	{
		TArray<FName> Rows = TeamMaterialDatabase->GetRowNames();
		
		for (int i = 0; i < Rows.Num(); i++)
		{
			FTeamMaterialsRow* curRow = TeamMaterialDatabase->FindRow<FTeamMaterialsRow>(Rows[i], FString("Team Material DB"));

			if (curRow)
			{
				if (curRow->ElementType == TeamType)
				{
					return curRow->MaterialReferenece;
				}
			}
		}
	}
	
	return nullptr;
}

void AHexPlanetGameState::SpawnAbilities()
{
	if (AbilityDatabase)
	{
		TArray<FName> Rows = AbilityDatabase->GetRowNames();

		for (int i = 0; i < Rows.Num(); i++)
		{
			FAbilityRow* curRow = AbilityDatabase->FindRow<FAbilityRow>(Rows[i], FString("Ability DB"));

			if (curRow)
			{
				SpawnAbility(curRow->BPRef);
			}
		}
	}
}

void AHexPlanetGameState::CalculateScores()
{
	TArray<AActor*> FoundActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), APlanetTriangle::StaticClass(), FoundActors);

	for (int i = 0; i < TeamScores.Num(); i++)
	{
		TeamScores[i].TotalScore = 0;
	}

	for (int i = 0; i < FoundActors.Num(); i++)
	{
		APlanetTriangle* curTriangle = Cast<APlanetTriangle>(FoundActors[i]);
		if (curTriangle)
		{
			EElement curTeamElement = curTriangle->ElementOwner;
			if (curTeamElement == EElement::NEUTRAL)
			{
				continue;
			}

			int Index = -1;
			GetTeamScore(curTeamElement, Index);
			
			if (Index != -1 && Index < TeamScores.Num())
			{
				TeamScores[Index].TotalScore += 1;
			}
		}
	}

	TeamScores.Sort([](const FTeamScore& A, const FTeamScore& B)
	{
		int32 ScoreA = A.TotalScore;
		int32 ScoreB = B.TotalScore;

		return ScoreA > ScoreB;
	});

	for (int i = 0; i < Teams.Num(); i++)
	{
		TArray<AHexPlanetPlayerState*> Players = Teams[i]->GetTeamPlayers();
		for (int j = 0; j < Players.Num(); j++)
		{
			AHexPlanetCharacter* curChar = Players[j]->PawnRef;
			if (curChar)
			{
				AHexPlanetPlayerController* curController = Cast<AHexPlanetPlayerController>(curChar->GetController());
				if (curController)
				{
					curController->NotifyUI(eUINotifyEvent::TURN_DONE);
				}
			}
			
		}
	}
}

void AHexPlanetGameState::NextPlayerTurn()
{
	Server_NextPlayerTurn();
}

bool AHexPlanetGameState::Server_NextPlayerTurn_Validate()
{
	return true;
}

void AHexPlanetGameState::Server_NextPlayerTurn_Implementation()
{
	if (Teams.Num() == 0)
	{
		return;
	}

	if (this->PlayerArray.Num() == 0)
	{
		return;
	}
	
	//First turn
	if(TurnBased_GameStatus.ActiveTeamTurn == nullptr && TurnBased_GameStatus.ActivePlayer == nullptr)
	{
		int32 NextTeamIndex = 0;
		int TeamPlayerCount = Teams[NextTeamIndex]->GetTeamPlayers().Num();

		while (TeamPlayerCount == 0)
		{
			NextTeamIndex += 1;
			if (Teams.Num() > NextTeamIndex)
			{
				TeamPlayerCount = Teams[NextTeamIndex]->GetTeamPlayers().Num();
			}
			else
			{
				NextTeamIndex = 0;
				TeamPlayerCount = Teams[NextTeamIndex]->GetTeamPlayers().Num();
			}
		}

		TurnBased_GameStatus.ActiveTeamTurn = Teams[NextTeamIndex];
		TurnBased_GameStatus.ActivePlayer = TurnBased_GameStatus.ActiveTeamTurn->GetTeamPlayers()[0];
	}
	else
	{
		AElementalTeam* curTeam = TurnBased_GameStatus.ActiveTeamTurn;
		AHexPlanetPlayerState* curPlayer = TurnBased_GameStatus.ActivePlayer;

		int32 NextPlayerIndex = GetPlayerInTeamIndex(curTeam, curPlayer) + 1;
		
		
		if (curTeam->GetTeamPlayers().Num() > NextPlayerIndex)
		{
			TurnBased_GameStatus.ActivePlayer = curTeam->GetTeamPlayers()[NextPlayerIndex];
		}
		else
		{
			int32 NextTeamIndex = GetTeamIndex(curTeam->ElementType) + 1;
			
			if (Teams.Num() <= NextTeamIndex)
			{
				NextTeamIndex = 0;
				TurnBased_GameStatus.TurnNumber++;
			}

			int TeamPlayerCount = Teams[NextTeamIndex]->GetTeamPlayers().Num();

			while (TeamPlayerCount == 0)
			{
				NextTeamIndex += 1;
				if (Teams.Num() > NextTeamIndex)
				{
					TeamPlayerCount = Teams[NextTeamIndex]->GetTeamPlayers().Num();
				}
				else
				{
					TurnBased_GameStatus.TurnNumber++;
					NextTeamIndex = 0;
					TeamPlayerCount = Teams[NextTeamIndex]->GetTeamPlayers().Num();
				}
			}

			TurnBased_GameStatus.ActiveTeamTurn = Teams[NextTeamIndex];
			TurnBased_GameStatus.ActivePlayer = Teams[NextTeamIndex]->GetTeamPlayers()[0];
		}
	}
}

int32 AHexPlanetGameState::GetTeamIndex(EElement ElementType)
{
	for (int i = 0; i < Teams.Num(); i++)
	{
		if (Teams[i]->ElementType == ElementType)
		{
			return i;
		}
	}

	return -1;
}

int32 AHexPlanetGameState::GetPlayerInTeamIndex(AElementalTeam* ChosenTeam, AHexPlanetPlayerState* ChosenPlayer)
{
	TArray<AHexPlanetPlayerState*> Players = ChosenTeam->GetTeamPlayers();

	for (int i=0; i < Players.Num(); i++)
	{
		if (Players[i] == ChosenPlayer)
		{
			return i;
		}
	}

	return -1;
}

AElementalAbility* AHexPlanetGameState::FindAbility(EAbilityType AbilityType, EElement ElementType)
{
	for (int i = 0; i < Abilities.Num(); i++)
	{
		if (Abilities[i]->ElementType == ElementType && Abilities[i]->AbilityType == AbilityType)
		{
			return Abilities[i];
		}
	}

	return nullptr;
}

bool AHexPlanetGameState::SpawnAbility_Validate(TSubclassOf<AElementalAbility> ChosenAbility)
{
	return true;
}

AElementalTeam* AHexPlanetGameState::FindPlayerTeam(AHexPlanetPlayerState* ChosenPlayer)
{
	for (int i = 0; i < Teams.Num(); i++)
	{
		if (Teams[i])
		{
			TArray<AHexPlanetPlayerState*> curTeam = Teams[i]->GetTeamPlayers();
			for (int j = 0; j < curTeam.Num(); j++)
			{
				if (ChosenPlayer == curTeam[j])
				{
					return Teams[i];
				}
			}
		}
	}

	return nullptr;
}

void AHexPlanetGameState::SpawnAbility_Implementation(TSubclassOf<AElementalAbility> ChosenAbility)
{
	FVector Location(0.0f, 0.0f, -500.0f);
	FRotator Rotation(0.0f, 0.0f, 0.0f);
	FActorSpawnParameters SpawnInfo;
	SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	UClass* curClass = ChosenAbility;
	AElementalAbility* curAbility = GetWorld()->SpawnActor<AElementalAbility>(curClass, Location, Rotation, SpawnInfo);
	
	if (curAbility)
	{
		Abilities.Add(curAbility);
	}
}

URuntimeMeshComponent* AHexPlanetGameState::GetTriangleMeshComponent(int MeshIndex)
{
	if(ActivePlanet)
	{
		for (int i = 0; ActivePlanet->PlanetMeshes.Num(); i++)
		{
			URuntimeMeshComponent* curComp = ActivePlanet->PlanetMeshes[i]->MeshComponent;
			if (curComp)
			{
				APlanetTriangle* curTriangle = Cast<APlanetTriangle>(curComp->GetOwner());
				if (curTriangle->UniqueTag == MeshIndex)
				{
					return curComp;
				}
			}
		}
	}
	return nullptr;
}