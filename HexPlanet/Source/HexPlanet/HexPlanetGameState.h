// Copyright (c) 2018+ True Imagination

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameState.h"
#include "UnrealNetwork.h"
#include "HexPlanetEnums.h"
#include "HexPlanetStructs.h"
#include "HexPlanetPlayerState.h"
#include "HexPlanetDataTables.h"
#include "Gameplay/ElementalTeam.h"
#include "Engine/DataTable.h"
#include "Gameplay/ElementalAbility.h"
#include "RuntimeMeshComponent.h"
#include "HexPlanetGameState.generated.h"

class APlanetoid;

UCLASS()
class HEXPLANET_API AHexPlanetGameState : public AGameStateBase
{
	///Default GameState Stuff
	
	GENERATED_BODY()
	AHexPlanetGameState();	
protected:
	virtual void BeginPlay() override;
	
	
	
	
	/// Active Gameplay
public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "HexPlanet|Gameplay")
	ECustomGameType ActiveGameType = ECustomGameType::TURN_BASED;

	///Turn Based
	UPROPERTY(Replicated, EditInstanceOnly, BlueprintReadOnly, Category = "HexPlanet|Gameplay")
	FTurnBased_GameStatus TurnBased_GameStatus;

	//UPROPERTY(Replicated, EditInstanceOnly, BlueprintReadOnly, Category = "HexPlanet|Gameplay")
	//AGameSettings GameSettings;

	UFUNCTION(BlueprintCallable, Category = "HexPlanet|Gameplay")
	void CalculateScores();

	UFUNCTION(BlueprintCallable, Category = "HexPlanet|Gameplay")
	void NextPlayerTurn();

	UFUNCTION(server, reliable, WithValidation, BlueprintCallable, Category = "HexPlanet|Gameplay")
	void Server_NextPlayerTurn();

	///Teams
public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "HexPlanet|Databases")
	UDataTable* TeamMaterialDatabase;

	UFUNCTION()
	FORCEINLINE TArray<AElementalTeam*> GetTeams() { return Teams; }

	UFUNCTION()
	TArray<APlanetTriangle*> GetAllTeamOwnedMeshes(EElement TeamElement);

	UFUNCTION(server, reliable, WithValidation)
	void AddPlayerToTeam(EElement ChosenElementType, AHexPlanetPlayerState* ChosenPlayer);

	UFUNCTION(server, reliable, WithValidation)
	void RemovePlayerFromTeam(EElement ChosenElementType, AHexPlanetPlayerState* ChosenPlayer);

	UFUNCTION(server, reliable, WithValidation)
	void ChangePlayerTeam(EElement ChosenElementType, AHexPlanetPlayerState* ChosenPlayer);

	UFUNCTION(BlueprintPure)
	AElementalTeam* FindTeam(EElement ElementType);

	UFUNCTION()
	int32 GetTeamIndex(EElement ElementType);

	UFUNCTION()
	int32 GetPlayerInTeamIndex(AElementalTeam* ChosenTeam, AHexPlanetPlayerState* ChosenPlayer);

	UFUNCTION()
	AElementalTeam* FindPlayerTeam(AHexPlanetPlayerState* ChosenPlayer);

	UFUNCTION()
	UMaterialInterface* GetTeamMaterial(EElement TeamType);
private:
	UPROPERTY(Replicated, VisibleAnywhere)
	TArray<AElementalTeam*> Teams;

	UFUNCTION(server, reliable, WithValidation)
	void CreateTeam(EElement ChosenElementType, FColor ChosenColor);

	///Abilities
public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "HexPlanet|Databases")
	UDataTable* AbilityDatabase;

	UFUNCTION()
	AElementalAbility* FindAbility(EAbilityType AbilityType, EElement ElementType);

	UFUNCTION(server,reliable,WithValidation)
	void SpawnAbility(TSubclassOf<AElementalAbility> ChosenAbility);
private:
	UPROPERTY(Replicated, VisibleAnywhere)
	TArray<AElementalAbility*> Abilities;

	void SpawnAbilities();

	///Score
public:
	UFUNCTION(BlueprintPure)
	FTeamScore GetTeamScore(EElement ElementType, int &Index);

	UFUNCTION(BlueprintPure)
	FORCEINLINE TArray<FTeamScore> GetTeamScores() { return TeamScores; }
private:
	UPROPERTY(Replicated, EditDefaultsOnly, Category = "HexPlanet")
	TArray<FTeamScore> TeamScores;

	///Planetoid
public:
	UPROPERTY(Replicated, VisibleAnywhere, Category = "HexPlanet")
	APlanetoid* ActivePlanet;

	UFUNCTION()
	URuntimeMeshComponent* GetTriangleMeshComponent(int MeshIndex);
};
